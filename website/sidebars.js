module.exports = {
  docs: [


        'Introduction',


    {
      type: 'category',
      label: 'NGS-analyses',
      items: [
        'NGS-analyses/NGS-intro',
        'NGS-analyses/RNA-seq-analysis',
        'NGS-analyses/BS-seq-analysis',
        'NGS-analyses/ChIP-seq-analysis',
        'NGS-analyses/ATAC-seq-analysis',
        'NGS-analyses/GEO-upload'
      ],
    },
    {
      type: 'category',
      label: 'Biostatistics',
      items: [
        {
          type: 'category',
          label: 'Biodiversity',
          items: [
            'Biostatistics/Biodiversity/traditional',
            'Biostatistics/Biodiversity/phylogenetic'
          ],
        }
      ],
    },
    {
      type: 'category',
      label: 'Docker',
      items: [
        'Docker/docker-intro'
      ],
    },
    {
      type: 'category',
      label: 'Website Tutorial',
      items: [
        'getting-started',
        'create-a-page',
        'create-a-document',
        'create-a-blog-post',
        'markdown-features',
        'website-analysis',
        'website-visible',
        'website-comment',
        'website-search',
        'thank-you',
      ],
    },
  ],
};
