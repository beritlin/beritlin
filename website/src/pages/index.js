// import React from 'react';
// import clsx from 'clsx';
// import Layout from '@theme/Layout';
// import Link from '@docusaurus/Link';
// import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
// import useBaseUrl from '@docusaurus/useBaseUrl';
// import styles from './styles.module.css';

import React from 'react';
// import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './styles.module.css';
// import HomepageFeatures from '@site/src/components/HomepageFeatures';
import Translate, {translate} from '@docusaurus/Translate';
import useBaseUrl, {useBaseUrlUtils} from '@docusaurus/useBaseUrl';
import { Socials } from '../components/Socials';



function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
      <div className={styles.hero} data-theme="dark">
          <div className={styles.heroInner}>
              <h1 className={styles.heroProjectTagline}>
                  <img
                      alt={translate({message: 'Docusaurus with Keytar'})}
                      className={styles.heroLogo}
                      src={useBaseUrl('/img/homepage.png')}
                      width="500"
                      height="500"
                  />
                  <span
                      className={styles.heroTitleTextHtml}
                      dangerouslySetInnerHTML={{
                          __html: translate({
                              id: 'homepage.hero.title',
                              message:
                                  '<b>Hello, I\'m </b> <br/> Pei-Yu <br/>   <b>Find out more about me!</b>',
                              description:
                                  'Home page hero title, can contain simple html tags',
                          }),
                      }}
                  />
              </h1>
              <br></br>
               <Socials /> 
              <div className={styles.indexCtas}>
                  <Link className="button button--primary" to="/docs">
                      Docs
                  </Link>
                  <Link className="button button--primary" to="/blog">
                      Blog
                  </Link>
              </div>
          </div>
      </div>
  );
}





export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
      <Layout
          title={`Hello from ${siteConfig.title}`}
          description="Description will go into a meta tag in <head />">
          <HomepageHeader/>
          <main>
          
              {/*<HomepageFeatures/>*/}
          
          </main>


          
      </Layout>
  );
}
