import React from "react";
import Layout from "@theme/Layout";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
// import styles from "./styles.module.scss";
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

import { Highlight, Prism } from "prism-react-renderer";
(typeof global !== "undefined" ? global : window).Prism = Prism;
require("prismjs/components/prism-rust");

// import AboutFeatures from "../components/AboutFeature";
// import Features from "../components/Features";
import Hero from "../components/me";
// import tab from "../components/resume/Tabs";
import Examples from "../components/Tabs";
// import Features from "@theme/Features";

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  const { tagline } = siteConfig;

  return (
      <Layout description={tagline}>
        <Hero />

        <main >
          {/* <Features />  */}
           {/* <AboutFeatures /> */}
           <Examples />


        </main>
      </Layout>
  );
}

export default Home;