import React from "react";
import clsx from "clsx";
import useBaseUrl from "@docusaurus/useBaseUrl";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";

 import styles from "./styles.module.css";

function Hero() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;

  return (
    <header id="hero" className={clsx("hero", styles.banner)} >
      <div className="container">
        <img
          src={useBaseUrl(`img/me.png`)}
          alt="Logo"   className={styles.logo}   />

        <h1 className="hero__title">
            {siteConfig.title}
            </h1>
        <p className={clsx("hero__subtitle", styles.subtitle)}>
          {siteConfig.tagline}
        </p>


        {/* <div 
        className={clsx(styles.buttons, styles.githubStars)}
        >
          <iframe
            className={styles.githubStarsButton}
            src="https://ghbtns.com/github-btn.html?user=beritlin&amp;repo=beritlin&amp;type=star&amp;count=true&amp;size=large"
            width={160}
            height={30}
            title="GitHub Stars"
          />
        </div> */}
      </div>
    </header>
  );
}

export default Hero;