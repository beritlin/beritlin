import React from "react";
// import CodeSnippet from "../Resume";
import Tabs from "@theme/Tabs";
import TabItem from "@theme/TabItem";
// import { Project, Experience, Education } from '.';
// import { projects, experience, education } from '../../utils/data';
import useBaseUrl, {useBaseUrlUtils} from '@docusaurus/useBaseUrl';
// import { resumeStyles } from 'styles.module.ts';
import styles from './styles.module.css';
import { TechStacks } from './TechStacks';


// import Headline from "@theme/Headline";
// import snippets from "./resume";
// import styles from "./styles.module.scss";










function renderTabs() {
  return (
    // <Tabs
    //   defaultValue={snippets[0].label}
    //   values={snippets.map((props, idx) => {
    //     return { label: props.label, value: props.label };
    //   })}
    // >
    //   {snippets.map((props, idx) => (
    //     <TabItem key={idx} value={props.label}>
    //       <CodeSnippet key={idx} {...props} />
    //     </TabItem>
    //   ))}
    // </Tabs>

      <Tabs>
        <TabItem value="resume" label="Resume" default>
         <h2>Experiences</h2>


         <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.fullWidth}>
            <div className={styles.imgContainer}>

                <img src={useBaseUrl('/img/AS.png')} className={styles.img} />

                <></>

            </div>
            <div className={styles.companyContainer}>
              <h3 className={styles.jobTitle}>Assistant R&D Scientist</h3>
              <span className={styles.company}>Institute of Plant and Microbial Biology, Academia Sinica</span>
              <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Feb 2025 - present </span>
                </h4>
                {/* <h4 className={styles.date}>
                  <span>   (2.5 yr) </span>
                </h4> */}
              </div>
              <div style={{ display: 'block', fontSize: 10 }}>
                <span>Taipei, Taiwan</span>
              </div>
            </div>
          </div>

          <div className={styles.city}>
            <p className={styles.information}>

                  {/* <>
                    • Adopted a novel method for fungal genomic research and reach 90% accuracy in essential gene prediction<br />
                    • Analysed the NGS samples mostly ATAC-seq and RNA-seq<br />
                    • Developed and maintain the programmes for bioinformatic analyses <br />
                    • Wrote research reports and papers<br />
                    • Gave lectures in the universities for graduated students about NGS analyses  <br />
                    • Maintained and managed the four servers and lab website <br />
                    • Recruited new lab members and mentored undergraduate students  <br />
                  </> */}

            </p>
            {/* <TechStacks stack={[ 'Bioinformatics','Biostatistics', 'Epigenetics' , 'R', 'Python']} />   */}
          </div>
        </div>
      </div>  


<div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.fullWidth}>
            <div className={styles.imgContainer}>

                {/* <img src={useBaseUrl('/img/AS.png')} className={styles.img} /> */}

                <></>

            </div>
            <div className={styles.companyContainer}>
              <h3 className={styles.jobTitle}>Research Assistant (RA)</h3>
              <span className={styles.company}>Institute of Plant and Microbial Biology, Academia Sinica</span>
              <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Nov 2019 - Jan 2025 </span>
                </h4>
                {/* <h4 className={styles.date}>
                  <span>   (2.5 yr) </span>
                </h4> */}
              </div>
              <div style={{ display: 'block', fontSize: 10 }}>
                <span>Taipei, Taiwan</span>
              </div>
            </div>
          </div>

          <div className={styles.city}>
            <p className={styles.information}>

                  {/* <>
                    • Adopted a novel method for fungal genomic research and reach 90% accuracy in essential gene prediction<br />
                    • Analysed the NGS samples mostly ATAC-seq and RNA-seq<br />
                    • Developed and maintain the programmes for bioinformatic analyses <br />
                    • Wrote research reports and papers<br />
                    • Gave lectures in the universities for graduated students about NGS analyses  <br />
                    • Maintained and managed the four servers and lab website <br />
                    • Recruited new lab members and mentored undergraduate students  <br />
                  </> */}

            </p>
            <TechStacks stack={[ 'Bioinformatics','Biostatistics', 'Epigenetics' , 'R', 'Python']} />  
            <br/>
            <br/>
          </div>
        </div>
      </div>  



      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.fullWidth}>
            <div className={styles.imgContainer}>

                <img src={useBaseUrl('/img/zoo.png')} className={styles.img} />

                <></>

            </div>

            
            <div className={styles.companyContainer}>
              <h3 className={styles.jobTitle}>Intern</h3>
              <span className={styles.company}>Conservation Centre, Taipei Zoo</span>
              <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Jul 2016 - Aug 2016  </span>
                </h4>
                {/* <h4 className={styles.date}>
                  <span>   (2.5 yr) </span>
                </h4> */}
              </div>
              <div style={{ display: 'block', fontSize: 10 }}>
                <span>Taipei, Taiwan</span>
              </div>
            </div>
          </div>

          <div className={styles.city}>
            <p className={styles.information}>

                  {/* <>
                    • Designed and implemented behavioural enrichment strategies for sun bears<br />
                    • Wrote and present research reports
                  </> */}

            </p>
            <TechStacks stack={[ 'Ecology' , 'Conservation', 'Behaviour' , 'Bears', 'Zoology']} />  
          </div>
        </div>
      </div>  

      <br/>


         <h2>Education</h2>

         <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.fullWidth}>
            <div className={styles.imgContainer}>
              <img  src={useBaseUrl('/img/aberdeen.png')} className={styles.img} />
            </div>
            <div className={styles.companyContainer}>
              <h3 className={styles.jobTitle}>University of Aberdeen</h3>
              <span className={styles.company}>MSc in Ecology and Conservation</span>
              <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Aug 2018 - Jun 2019</span>
                </h4>
              </div>
              <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Influences of Social and Spacing System on the Subadult of Townsend’s Vole (Microtus townsendii)</span>
                </h4>
              </div>
            </div>
          </div>
        </div>
      </div>
<br/>
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.fullWidth}>
            <div className={styles.imgContainer}>
              <img  src={useBaseUrl('/img/fju.png')} className={styles.img} />
            </div>
            <div className={styles.companyContainer}>
              <h3 className={styles.jobTitle}>Fu Jen Catholic University</h3>
              <span className={styles.company}>BSc in Life Science</span>
              <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Sep 2014 - Jun 2018</span>
                </h4>
              </div>
              <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Olfactory Cues to Elicit Aggressive Response of Two Gall-forming Aphids in Hormaphidinae</span>
                </h4>
              </div>
            </div>
          </div>
        </div>
      </div>







        </TabItem>
        <TabItem value="project" label="Projects" >
        <p></p>

    <div className={styles.container_p}>
      <div className={styles.imgContainer_p}>
        <img  src={useBaseUrl('/img/FunCore.png')} className={styles.img_p} />
      </div>
      <div className={styles.content_p}>
        <div className={styles.titleContainer_p}>
          <span className={styles.title_p}>Fungal Genomics</span>
        </div>
        <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Main project | Nov 2019 - Present</span>
                </h4>
        </div>
        <span className={styles.description}>Predict fungal essential gene through core gene identification</span> <br/>
        <span className={styles.description}>Predict biosynthesis gene clusters based on essential genes</span>
        <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Collaboration | May 2021 - Present</span>
                </h4>
        </div>
        <span className={styles.description}>Identify actin biosynthesis-related of Antrodia cinnamon</span>
        <br/>
         <TechStacks stack={['Fungi', 'Genomics', 'Core genes', 'Essential genes', 'BGC']} />
      </div>
      </div>

    

      <div className={styles.container_p}>
      <div className={styles.imgContainer_p}>
        <img  src={useBaseUrl('/img/maize.png')} className={styles.img_p} />
      </div>
      <div className={styles.content_p}>
        <div className={styles.titleContainer_p}>
          <span className={styles.title_p}>Maize (Epi)genomics</span>
        </div>
        <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Jun 2022 - Present</span>
                </h4>
        </div>
        <span className={styles.description}>Validate a new maize ATAC-seq protocol</span>
        <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Collaboration | Jun 2021 - Present</span>
                </h4>
        </div>
        <span className={styles.description}>Find maize ufo1 related DEG</span>
        <br/>
         <TechStacks stack={['ATAC-seq', 'RNA-seq', 'Zea Maize']} />
      </div>
      </div>


      <div className={styles.container_p}>
      <div className={styles.imgContainer_p}>
        <img  src={useBaseUrl('/img/tools.png')} className={styles.img_p} />
      </div>
      <div className={styles.content_p}>
        <div className={styles.titleContainer_p}>
          <span className={styles.title_p}>Methylation Analyses</span>
        </div>
        <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>Dec 2021 - Present</span>
                </h4>
        </div>
        <span className={styles.description}>Construct a tool for calculating methtylaion heterogeneity</span>
        <div className={styles.flex}>
                <h4 className={styles.date}>
                  <span>OCt 2022 - Present</span>
                </h4>
        </div>
        <span className={styles.description}>Improve a tool for methylation analyses</span>
        <br/>
         <TechStacks stack={['Methylation heterogeneity', 'Mathematical model', 'Biodiversity framework']} />
      </div>
      </div>



      

        <p></p>
        </TabItem>

        <TabItem value="publications" label="Publications" >
        <a href="https://orcid.org/0000-0003-0255-5467"><span><img  src={useBaseUrl('https://static-00.iconduck.com/assets.00/orcid-icon-2048x2048-l0zosc0u.png')} className={styles.img_id2} />  
        0000-0003-0255-5467      </span> </a>
        <a href="https://scholar.google.com/citations?user=m5ACEZUAAAAJ&hl=zh-TW"><span>        <img  src={useBaseUrl('https://icons.iconarchive.com/icons/academicons-team/academicons/256/google-scholar-square-icon.png')} className={styles.img_id2} />
        Google Scholar</span></a>  <br/>



        <p></p>
        <span className={styles.company}>*co-first author</span> <br/>
          <p></p>
      <h2>Publications</h2>
      <div className={styles.container}>
        <div className={styles.content_c}>
          <div className={styles.fullWidth}>
              <span className={styles.company}> JW. A. Hsieh*, <b>PY. Lin*</b>, CT. Wang, YJ. Lee, P. Chang, R. JH. Lu, PY. Chen and CJ. R. Wang (2024) 
              Establishing an optimized ATAC-seq protocol for the maize
                <i> Frontiers in Plant Science, Sec. Plant Genetics, Epigenetics and Chromosome Biology, Volume 15</i>, 
                https://doi.org/10.3389/fpls.2024.1370618</span>
                <p></p> 
             <span className={styles.company}> <b>PY. Lin*</b> YT. S. Chang, YC. Hunag and PY. Chen (2023) 
               There’s More to It: Uncovering Genomewide DNA Methylation Heterogeneity
              <i> Epigenetics & Chromatin</i>, 
              https://doi.org/10.1186/s13072-023-00521-7</span>
              <p></p> 
             <span className={styles.company}> HT Lee, <b>PY. Lin</b> and PY. Chen (2023) 
               There’s More to It: Uncovering Genomewide DNA Methylation Heterogeneity
              <i> Epigenomics</i>, 
              https://doi.org/10.2217/epi-2023-0228</span>
              <p></p> 
              <span className={styles.company}> R. JH. Lu*, <b>PY. Lin*</b>, MR. Yen*, BH. Wu and PY. Chen (2023) 
               MethylC-analyzer: a comprehensive downstream pipeline for the analysis of genome-wide DNA methylation
              <i> Botanical studies</i>, 
              https://doi.org/10.1186/s40529-022-00366-5</span>
              <p></p> 
              <span className={styles.company}> <b>PY. Lin</b> and PY. Chen (2021) 
              Review of: Deep-BGCpred: A unified deep learning genome-mining framework for biosynthetic gene cluster prediction.
              <i> Qeios</i>, 
              https://doi.org/10.32388/14X8U9</span>
              <p></p> 
            </div>
        </div>
      </div>


      <h2>Book chapter</h2>
      <div className={styles.container}>
        <div className={styles.content_c}>
          <div className={styles.fullWidth}>
            <span className={styles.company}> <b>PY. Lin</b>, KL. Chen, GJ. Lin, SC. Huang and PY. Chen (2024) 
              Bioinformatics Analysis of DNA Methylation.
              <i> Methods in Molecular Biology</i>,
               Accepted</span>
              <p></p> 
              <span className={styles.company}> CY. L. Sheu*, YC. Huang*, <b>PY. Lin</b> , GJ. Lin and PY. Chen (2024) 
              Bioinformatics of Epigenetic Data Generated from Next-Generation Sequencing, chapter 4.
              <i> Elsevier in Translational Epigenetics</i>, 
              3 edition, ISBN: 9780443218125</span>
              <p></p> 
            </div>
        </div>
      </div>



      <h2>Conferences</h2>
        <div className={styles.container}>
        <div className={styles.content_c}>
          <div className={styles.fullWidth}>
             <span className={styles.company}> <b>2023 - Jul : </b>
              Identification of Core Genes in Fungal Genomes Assisting the Discovery of Essential Genes.
              <i> oral session presented at 2023 MSA Annual Meeting: Elevating Mycology</i>, 
              Flagstaff, Arizona, USA</span><br/>

              <span className={styles.company}> <b>2022 - Jul : </b>
              Estimating Genome-wide DNA Methylation Heterogeneity with Methylation Patterns. 
              <i> oral session presented at The 31th South Taiwan Statistic Conference</i>, 
              Taichung, Taiwan</span><br/>
              
              <span className={styles.company}> <b>2021 - Dec : </b>
              500 Fungal Genomes: Predicting Essential Genes through Core Genes by Network Statistics. 
              <i> poster session presented at Academic Advisory Committee Meeting</i>, 
              online</span><br/>
 
               <span className={styles.company}> <b>2021 - Nov : </b>
              A network-based method for predicting fungal essential genes through identification of core genes. 
              <i> oral presented at Annual Meeting of Taiwan Mycological Society</i>, 
              online</span><br/>
               
              <span className={styles.company}> <b>2021 - Oct : </b>
              A network-based method for predicting fungal essential genes through identification of core genes. 
              <i> oral presented at The 30th South Taiwan Statistic Conference</i>, 
              Kaohsiung, Taiwan</span><br/>
              
              <span className={styles.company}> <b>2021 - Jul : </b>
              500 Fungal Genomes: Finding Essential Genes based on Core Genes Prediction.
              <i> poster session presented at Botany 2021 Virtual</i>, 
              online</span><br/>
              
              <span className={styles.company}> <b>2021 - Apr : </b>
              Finding Potential Drug Targets of Pathogenic Fungi by Fungal Essential Genes Through Core Genes Prediction. 
              <i> poster session presented at Annual Meeting of Taiwan Agronomy Society</i>, 
              Chiayi, Taiwan</span><br/>
              
              <span className={styles.company}> <b>2020 - Oct : </b>
              FunCore: A Database for Fungal Core Genes. 
              <i> poster and short talk session presented at Annual Meeting of Taiwan Mycological Society</i>, 
              Taipei, Taiwan</span><br/>
             
              <span className={styles.company}> <b>2020 - Jul : </b>
              FunCore: A Database for Fungal Core Genes. 
              <i> poster session presented at Annual Meeting of Mycological Society of America</i>, 
              online</span><br/>
              
              <span className={styles.company}> <b>2018 - Mar : </b>
              Olfactory Cues to Elicit Aggressive Response of Two Gall-forming Aphids in Hormaphidinae. 
              <i> poster session presented at Seventh International Symposium on Cecidology</i>, 
              Nantou, Taiwan</span><br/>
              
              <span className={styles.company}> <b>2017 - Jan : </b>
              Behavioural Enrichment Strategies of Sun Bear (Helarctos malayanus) in Taipei Zoo.
              <i> poster session presented at 2017 Congress of Animal Behavior and Ecology</i>, 
              Kaohsiung, Taiwan</span><br/>
              

          </div>
        </div>
      </div>

        </TabItem>
      </Tabs>


  );
}

function Examples() {
  return (
    <>
      {/* {snippets && snippets.length && ( */}
        <section id="examples" >
          <div className="container">
            <div className="row">
              <div className="col col--10 col--offset-1">
                {/* <Headline
                  category="Examples"
                  title="Quick snippets to get started with Country State City API"
                /> */}
                {renderTabs()}
              </div>
            </div>
          </div>
        </section>
      {/* )} */}
    </>
  );
}

export default Examples;