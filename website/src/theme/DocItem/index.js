import React from 'react';
import DocItem from '@theme-original/DocItem';
import { ReactCusdis } from 'react-cusdis'

export default function DocItemWrapper(props) {
  
  return (
    <>
      <DocItem {...props} />

         
         
         <ReactCusdis
          lang="en-UK" 
          attrs={{
          host: 'https://cusdis.com',
          appId: '533e8bce-f4a4-444d-98dd-ebc67463c352',
          pageId: "{{ PAGE_ID }}",
          pageTitle: props.title,
          pageUrl: 'https://peiyu.us/' + props.siteUrl,
          }}
          />


    </>
    

  );
}
