import React from 'react';
import BlogPostItem from '@theme-original/BlogPostItem';
import { ReactCusdis } from 'react-cusdis'

export default function BlogPostItemWrapper(props) {
  return (
    <>
      <BlogPostItem {...props} />

      
         
         <ReactCusdis
          lang="zh-tw" //繁體中文
          attrs={{
          host: 'https://cusdis.com',
          appId: '533e8bce-f4a4-444d-98dd-ebc67463c352',
          pageId: "{{ PAGE_ID }}",
          pageTitle: props.title,
          pageUrl: 'https://peiyu.us/' + props.siteUrl,
          }}
          />

    

    </>
  );
}
