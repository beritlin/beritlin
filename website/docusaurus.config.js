/** @type {import('@docusaurus/types').DocusaurusConfig} */
const math = require('remark-math');
const katex = require('rehype-katex');

module.exports = {
  title: 'Pei-Yu Lin',
  tagline: 'Hello, there',
  // url: 'https://beritlin.gitlab.io',
  // baseUrl: '/beritlin/',
  url: 'https://peiyu.us',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/pyllogo.png',
  organizationName: 'GitLab', // Usually your GitHub org/user name.
  projectName: 'beritlin', // Usually your repo name.

  themeConfig: {
    announcementBar: {
      id: 'support_us',
      content:
        '⭐️ Welcome to my website! ',
      backgroundColor: '#000000',
      textColor: '#FFFFFF',
      isCloseable: true,
    }, 
    

    navbar: {
      title: 'Pei-Yu',
      logo: {
        alt: 'My Site Logo',
        src: 'img/pyllogo.png',
      },
      items: [
        {
          to: '/about',
          activeBasePath: 'About',
          label: 'About',
          position: 'left',
        },
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/pages/docusaurus',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Facebook',
              href: 'https://www.facebook.com/berit.lin',
            },
            {
              label: 'Instergram',
              href: 'https://www.instagram.com/beritlin/',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/peiyulin11',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: 'blog',
            },
            {
              label: 'All Post',
              to: 'https://peiyu.us/blog/archive/',
            },
            {
              label: 'Blog category',
              to: 'https://peiyu.us/blog/tags/',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Pei-Yu Lin.`,
    },
    algolia: {
      appId: 'RE6HB1KTCA',
      apiKey: '3b08c0ee77e7f854414c871aedab2e67',
      indexName: 'peiyudocs',
    }
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          remarkPlugins: [math],
          rehypePlugins: [katex],

          sidebarPath: require.resolve('./sidebars.js'),
        },
        blog: {
          showReadingTime: true,
          blogSidebarTitle: '最新文章',
          blogSidebarCount: 15,

        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        gtag: {
          trackingID: 'G-9ZLPSNZYN3',
          anonymizeIP: true,
        },
        googleAnalytics: {
          trackingID: '358213898',
      },
        sitemap: {
          changefreq: 'weekly',
          priority: 0.5,
          filename: 'sitemap.xml',
      },
      },
    ],
  ],
  stylesheets: [
    {
      href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
      type: 'text/css',
      integrity:
        'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
      crossorigin: 'anonymous',
    },
  ]
};
