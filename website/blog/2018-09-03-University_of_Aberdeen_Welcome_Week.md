---
slug: University-of-Aberdeen-Welcome-Week 
title: 2018 || 留學日記 | 亞柏丁大學迎新週
tags: [Field trip, UniofAberdeen, Aberdeen, Scotland,UK, 留學日記,2018]
---
【2018.09.03-2018.09.07】亞伯丁大學迎新週

![title](https://linberit.files.wordpress.com/2018/09/816ce0e3-dae8-4f93-b126-97f4db2196ff.jpg?w=1472)

<!--truncate-->

度過了來亞伯丁的第一個星期，
迎新週其實是最輕鬆的一週，
課程都還沒開始，
所以可以有一個緩衝期來習慣、認識校園和把該辦的東西都辦好。
星期一二都是學校辦的介紹與演講，
想要參加可以上網申請想去的那一堂，
內容很廣泛，
從校園介紹、城市導覽到心理輔導（怕第一次離家的人想不開）都有，
但是我因為剛校外教學回來又有點懶惰加上很多東西還沒置辦好，
所以一堂都沒去…

因此，
星期一我是去郵局領BRP卡，這個很重要一定要先做！
和買基本家居用品（我前一天到宿舍發現連棉被枕頭都沒有）
搬這些重物來來回回跑幾次一天也是累壞了。
星期二我才第一次進到學校，

![](https://linberit.files.wordpress.com/2018/09/3fb2f360-ef60-4e49-a139-b5f0aa5ffc0a.jpg?w=393&h=221&zoom=2)
![](https://linberit.files.wordpress.com/2018/09/f83cfbc4-45dd-4aaf-9aed-2a0296fc95fb.jpg?w=393&h=221&zoom=2)
![](https://linberit.files.wordpress.com/2018/09/2119c633-8a32-42a1-9ba0-465085f7eed0.jpg?w=392&h=221&zoom=2)
亞伯丁是產花崗岩的地方，
因此建築物幾乎都是由花崗岩建成，
整個校園就是銀灰的色調
加上歷史悠久（大概500年）
看起來又有點哈利波特的感覺！
![](https://linberit.files.wordpress.com/2018/09/6ff2b44a-6a96-47a5-a549-ee25f7a9d96f.jpg?w=335&h=446&zoom=2)
而新建的圖書館就是很突兀地聳立在旁邊，
充滿現代設計感的建築和旁邊的國王學院真的很衝突！

![](https://linberit.files.wordpress.com/2018/09/d292c0cf-ce10-4864-ae16-e44d110f171d.jpg?w=166&h=221&zoom=2)
![](https://linberit.files.wordpress.com/2018/09/70f62cab-7ed0-45c4-8233-cb98df497b3c.jpg?w=166&h=221&zoom=2)
還好我線上註冊都已完成，
只要到info hub領取學生證、visa check、GP註冊和拿bank letter就好，
學校教職員都很熱心，
每個區旁邊都會有意見回饋機讓你按
我還發現他們員工自己有時候還幫自己按笑臉xD
下午我去銀行辦帳號，但是因為人太多銀行員叫我線上辦好再來找他們…
（後來線上辦好又沒成功，總之辦銀行帳號又是另一個麻煩的故事了）

![](https://linberit.files.wordpress.com/2018/09/c967e79b-e385-4a88-933b-7b8c671fa6bf.jpg?w=482&h=642)
星期三開始就是系所上自己的活動介紹，
一早就到學校集合、認識導師和系館，
我們系館大概是全校最醜的建築物我就不放了 QQ
不過系館裡面就很精彩，
竟然有自己的博物館，
裡面充滿了各種動物的標本，
如果有機會再來做深入介紹吧！

![](https://linberit.files.wordpress.com/2018/09/5d7716bb-5584-43cd-8224-47016b3ebe3e.jpg?w=341&h=256&zoom=2)
星期四我們部門也很用心，
安排了一個校外教學field trip，
又是跋山涉水的一天啊～
![](https://linberit.files.wordpress.com/2018/09/d443fed0-4930-4680-97c0-ee353210a626.jpg?w=191&h=256&zoom=2)
由於是自由參加，
原本我不是很想去，
但聽到可能可以看到洄游的鮭魚跳來跳去，
讓我心動才決定去參加！
結果我們什麼都沒看到呢～（欲哭無淚

![](https://linberit.files.wordpress.com/2018/09/400e89b9-9a76-404b-aaa0-ad334282adab.jpg?w=241&h=241&crop=1&zoom=2)
最後，迎新週最後一天也是滿滿的活動，
一早的生命科學部長演講，
中午的BBQ午餐，
下午的課程介紹到論文抄襲演講，
還有免費的盆栽可以拿 ❤
![](https://linberit.files.wordpress.com/2018/09/9424ac2c-435e-4b1d-b86e-954674a89236.jpg?w=241&h=241&crop=1&zoom=2)

總之一週就這麼過去了，
原本覺得迎新週怎麼可以這麼多事情，
後來才知道，
這週是最幸福的了～
開始上課後的課表朝九晚五，
比當初大一還要累人…