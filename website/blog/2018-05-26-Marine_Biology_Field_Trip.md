---
slug: Marine-Biology-Field-Trip 
title: 2018 || 大學日記 | 石門洞野採
tags: [Field trip, Marine, Taiwan,大學日記 ,2018]
---
【 2018.05.26 】海洋生物學戶外教學 – 石門洞野採

![title](https://linberit.files.wordpress.com/2018/07/img_2862.jpg?w=1472)

<!--truncate-->


這次去戶外教學

天氣很好、遊客很多

最低潮時間是下午3點左右

![me](https://linberit.files.wordpress.com/2018/07/img_2863.jpg?w=1472)

首先裝備非常重要

最好穿可下水的泳衣
* 穿包起你腳的鞋子
* 抓生物時請戴手套
* 帶大網子、小網子、小湯匙
* 暫時裝採集到生物的小飼養盒
* 注意防曬，但為了海洋碰水的地方不要擦防曬乳
 
![seaweed](https://linberit.files.wordpress.com/2018/07/e79fb3e893b4-e1530762415424.jpg?w=602)
一過洞口，第一眼看到的就是整片綠油油的石蓴（*Ulva lactuca*）
石蓴是台灣最常見的綠藻 ，在海邊看到要小心不要踩，因為他非常滑～
仔細翻一翻有時候都可以看到很多小生物藏在裡面

![](https://linberit.files.wordpress.com/2018/07/e895a8e897bb-e1530762880996.jpg?w=602&h=488)
和石蓴同一目，常見的還有網球藻（*Dictyosphaeria cavernosa*）
一顆一顆小小的非常可愛，通常都三五個生長在一起
蕨藻

![](https://linberit.files.wordpress.com/2018/07/e4b88de79fa5e98193e897bb-e1530763377443.jpg?w=608)
另一種常見的綠藻為蕨藻
有很多不同的形態
這次採集到的是盾狀的盾葉蕨藻（*Caulerpa peltata*）
旁邊和蕨藻纏在一起的是紅藻裡比較好辨認珊瑚藻中的異邊孢藻（*Marginisporum aberrans*）

![](https://linberit.files.wordpress.com/2018/07/33781976_10211061397392676_4674848825389613056_n-e1530763660549.jpg?w=626&h=422)
除了綠藻紅藻，也可以看到許多褐藻
像是馬尾藻家的成員
我不確定我們採到的是哪個，但我覺得比較接近粉葉馬尾藻（*Sargassum glaucescens*）

除此之外，石門洞的藻類多樣性很高
但可惜我實在不太會辨認啊

![](https://linberit.files.wordpress.com/2018/07/e4ba9ee6b4b2e581b4e88ab1e6b5b7e891b5-e1530763988382.jpg?w=620)
刺絲胞動物門的生物在石門洞也很好觀察到
最容易的應該就是海葵
很多石頭上都可以發現一顆一顆灰灰的突起
其實就是亞洲側花海葵（*Anthopleura asiatica*）
平時在退潮時環境就會像圖片這樣觸手緊縮呈圓筒狀
待漲潮後就會伸展出觸手來捕食魚類和小型動物
有許多紅色的小斑點在表面

![](https://linberit.files.wordpress.com/2018/07/e798a4e88f9fe891b5-e1530764316870.jpg?w=620)
再深一點的潮池也可以找到另一種海葵
群體生長的瘤菟葵（*Palythoa tuberculosa*）
通常都有毒 所以接觸時要小心

![](https://linberit.files.wordpress.com/2018/07/e78f8ae7919a-e1530764427607.jpg?w=618&h=426)
在潮間帶也可以找到這種粉色手指狀的軟珊瑚在岩縫之間

![](https://linberit.files.wordpress.com/2018/07/e79baae79aaee89498-e1530764785918.jpg?w=622)
棘皮動物也是潮間帶間常見的生物
海星、陽燧足通常都被遊客抓到斷肢
比較完整的應該只有海參
最容易發現的為盪皮參（*Holothuria leucospilota*）
通常都在有沙的岩石縫下拉成長長一條在覓食
抓起來後就會縮短 接著噴水
若還是感到緊迫 可能會把胃吐出來
那黏液大概跟三秒膠一樣麻煩處理

![](https://linberit.files.wordpress.com/2018/07/e4b88de79fa5e98193-e1530764926959.jpg?w=620&h=500)
還有採到另一種小海參
我猜應該是非洲異瓜參（*Afrocucumis africana*）
大概兩公分大 體色偏粉紫色
通常棲息在海草床中

![](https://linberit.files.wordpress.com/2018/07/e9b3b3e89ebae7a791-e1530765209967.jpg?w=620)
潮間帶的軟體動物更為多種
從沿岸就可以看到各種不同的螺聚集在一起（當然有些是寄居蟹）
但要注意這種芋螺（*Conus*）
通常都有漂亮的外表、顏色
但卻是海邊危險的生物代表之一
這一科的螺幾乎都是掠奪性的動物
因此會有異化的齒舌 且往往都具有神經毒素的毒腺

![](https://linberit.files.wordpress.com/2018/07/e9bb91e9bd92e789a1e8a0a3-e1530765492674.jpg?w=622&h=498)
而礁石上最危險的生物有兩種
一種就是群聚在岩石上的牡蠣
這裡的牡蠣應該是僧帽牡蠣（*Tetraclita kuroshioensis*）
長大一點的就是生蠔了～

![](https://linberit.files.wordpress.com/2018/07/e897a4e5a3ba-e1530765828978.jpg?w=628)
另一個危險生物就是藤壺
應該可以排在造成受傷的第一名
這裡看到的應該是黑潮笠藤壺（*Tetraclita kuroshioensis*）
擁有偏綠的外殼 退潮時會緊閉殼口以避免水分喪失


其他節肢動物最常見的就是寄居蟹
甚至附近攤販還有在販賣一隻20元
蠻糟糕的啊
再來就是大螃蟹小螃蟹各種螃蟹

![](https://linberit.files.wordpress.com/2018/07/e79fb3e89fb3efbc88e4babaefbc89-e1530766514747.jpg?w=622&h=444)
在低潮區附近的潮池都可以找到比較大的鈍齒短槳蟹（*Thalamita crenata*）
就是俗稱的石蟳
身體是深綠色 螯為藍色尖端處為紅色
相當兇猛 一定要用工具來抓
有很多遊客在抓 可能是要帶回家吃的吧


![](https://linberit.files.wordpress.com/2018/07/e89fb9-e1530766610442.jpg)
比較特別的是這隻綠色小螃蟹
老師說是隱蟹
但我完全找不到這是誰

![](https://linberit.files.wordpress.com/2018/07/e89da6-e1530767328655.jpg?w=624&h=456)
除了螃蟹 當然還有蝦
這種槍蝦（*Alpheus heterochaelis*）
有一對一大一小的蝦螯 在捕食時會把螯快速合上 噴出時速約100公里的水流
將獵物擊昏甚至殺死
這種高速水流引發的空穴現象 就會發出類似槍響的霹啪聲
與蝦虎魚通常都有共生關係

![](https://linberit.files.wordpress.com/2018/07/e6a29de7b48be9b088-e1530767837879.jpg?w=620)
蝦虎魚是潮間帶淺區常見的魚
還有魚尉、雀鯛等等
但是移動太快了 我都沒有撈到
只有撈到一直被誤認為雀鯛和蝶魚的柴魚（*Microcanthus strigatus*）
話說在查資料時 我拿去google以圖搜圖 他說這是海龜呢！

![](https://linberit.files.wordpress.com/2018/07/e9ad9a-e1530768123902.jpg?w=624)
還有這種豆仔魚
算是比較容易抓到的 在陽光照射下都波光粼粼
豆仔魚是多種鯔科魚類幼魚的總稱
用大網子撈很容易從網子縫隙逃掉 XD


除了上述的生物之外
也聽到有同學抓到藍點章魚
以前也有人抓過小石斑魚 海蛞蝓 海膽等等
**但觀察玩這些小生物之後
記得一定要將他們送大海**
讓他們能繼續在這裡好好生活下去！