---
slug: 2023-korea-last-day
title: 2023 || 韓國旅遊 | 北村韓屋村之八景走不完
tags: [美食,體驗,旅遊, 韓國旅遊,2023]
comments: true 
---

【 2023.04.06 韓國旅遊 第四天 腳已經廢掉但還是要走北村八景 】

原本今天要去汝矣島看櫻花，  
但一場大雨後櫻花都在地上了...  
最後決定去昨天取消的**北村韓屋村**拍拍照～

<img src="/img/blog/seoul-day4-0.png" width="100%"/>  

<!--truncate-->

> **第四日的詳細行程可以看**[**funliday**](https://www.funliday.com/berit/journals/80835)！

## 北村韓屋村
搭地鐵道**安國站**後，  
**二號出口**出去一直直直走，  
會看到**旅遊服務站**，  
服務人員很好的給我們地圖並介紹八個吃再哪還有哪裡可以**免費尿尿**！
<img src="https://img.poibank.com/6Fqa3wlF2WZ6OVuxwfprTpYG2xI=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMjA5NjgzMDEtMmFkOS00MGQ2LWJjMTAtNWQ4M2VhNjBlYWVmIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>  

因為最後一天腳真的已經有點跛腳了，  
加上天氣也還不是很好，只挑了熱點區踩點（**紅線**切西瓜），  
體力好的人可以走完全程（**藍線**）。

<img src="/img/blog/seoul-day4-1.png" width="100%"/>  

**北村四五六七景**很熱鬧都是人，  
也有不少人選擇是在這裡穿韓服，  
但當初我覺得這裡不夠古代不適合就選擇去景福宮穿越就好～

因為這裡是真的住家不是擺飾，  
所以在拍照逛逛的時候要注意音量，  
旁邊也都黃衣服小志工在提醒大家要**小聲**。
<img src="https://img.poibank.com/eKYT_gWbEhYB1qtPFZx57Ko_q6A=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNzU4YmYyNDQtZDg4Ny00ODI3LWIyM2EtOGM5OWNhZjRiYWYxIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>  


這裡的色調拍起來都很好看，  
可惜我臉好腫，  
出國真難避免**水腫**啊哭～

<img src="https://img.poibank.com/-Okeezhm0jejLRxCwWLe1xJ-3Vo=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNzY5NDBkN2ItZTgzNC00OTJlLTk2NjMtNWU5NjQ2MDg1MTIwIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>  
<img src="https://img.poibank.com/1_ljXkoJtcQ7WciMaOGmmdIbVTQ=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvOTllNWM3MmUtODQzYi00NGI2LTkxYzMtM2Q3MmM0NmI2NDU2In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>  
<img src="https://img.poibank.com/wIG3_sPtvR5fdxCB48lmu5ASE64=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYmIzNTAyZmUtZWMxZi00OGRlLTgyNDctNmZlMGU2NGIxZjY4In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 



## 三清洞
三清洞很多**文青咖啡廳**，  
但因為才剛吃完早餐加上旅伴沒特別愛咖啡廳，  
就沒有特別進去，有點可惜啊～  
不過漂亮的餐廳外面還是不錯拍啦！

<img src="https://img.poibank.com/Wek7AB8VxBVPY5xjoaqbPa163cU=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZWVlYzFiYWUtYjU4Zi00OTI3LWE3MjAtNTEzNWY0NzVmNzMzIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>  
<img src="https://img.poibank.com/h8C99pbzpNvKEVN9HOvHhxopzT0=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZjE1MGM1NzItNGQ4My00N2Y5LWIzZGEtOTRlMDAwOWRlZGFjIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>  

稍微繞繞走走，  
就離開北村和三清洞，  
跑去**新村逛**街吃飯，
再到**樂天超市**做最後採購就準備返家啦～


## ☺ 第四天總結
覺得玩不太夠，  
但已經體力不支了！

:::note 韓國旅遊相關

[韓服體驗＋景福宮](https://peiyu.us/blog/2023-korea-third-day)  
[韓式證件照＋南山首爾塔](https://peiyu.us/blog/2023-korea-second-day)  
[韓國校服體驗＋石村湖賞櫻](https://peiyu.us/blog/2023-korea-first-day)  
[韓國行前住宿交通換錢等須知](https://peiyu.us/blog/2023-korea-preparation)  
[韓國入境相關資料](https://peiyu.us/blog/2023-korea-keta-qcode)  
[韓國WOWPASS申請教學](https://peiyu.us/blog/2023-korea-wowpass)  

:::