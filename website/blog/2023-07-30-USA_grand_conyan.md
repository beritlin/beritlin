---
slug: 2023-usa-grand-canyon
title: 2023 || 美國旅遊 | 大峽谷南緣一日遊
tags: [體驗,旅遊, 美國旅遊,2023]
comments: true 
---

【 2023.07.30 大峽谷南緣一日遊 弗拉格斯塔夫出發 】

**大峽谷**這個地方應該大家聽起來都很熟悉，  
但我也沒想到我剛好有機會親自去看各個美景，  
畢竟我對美國沒有特別多的嚮往（歐洲派），  
對石頭、地理也沒特別多的愛，  
但站在大峽谷懸崖邊看到這片壯麗的風景，  
還是覺得很震撼。

<img src="/img/blog/usa-grand-canyon.png" width="100%"/>  

<!--truncate-->

[**大峽谷國家公園（Grand Canyon National Park）**](https://www.nps.gov/grca/planyourvisit/hours.htm)佔地遼闊﹐ 
由**科羅拉多河（Colorado River）**下切所形成，深達1,600公尺，最寬處達29公里， 
主要分為**北緣（North Rim）、南緣（South Rim）和西緣（West Rim）**﹐  
北緣大部分時間在積雪，冬天不開放，西緣不在國家公園內腹地較小，但有天空步道。  
南緣最熱門視野最開闊，同時觀光開發比較完善，  
有各種步道、接駁車、直升機和騾子步道﹐而且一年四季都開放。

> **詳細行程可以看**[**funliday**](https://www.funliday.com/berit/journals/80835)！

### 跟團
當然去大峽谷可以**自駕**也可以**跟團**，  
我是跟著會議安排的團出發，  

因為這次開會地點是在**弗拉格斯塔夫（Flagstaff）**，  
所以也是從這邊出發到大峽谷，  
走**藍色路線**前往，這條路線會經過另一個國家公園，  
回程是**紅色路線**，這裡的會經過不少印地安部落，  
弗拉格斯塔夫這條路線好像比較少國外觀光客走，  
大部分都是從比較熱鬧的拉斯維加斯出發。

<img src="/img/blog/usa-esta/grand_canyon_map.png" width="100%"/>  

跟團好處是有**專屬導遊解說**（記得要給小費），  
我們的導遊會帶望遠鏡配合解說比較不會但純的就是看地貌，  
還會多一些有趣小故事，  
最重要的是還有附**午餐**，畢竟裡面真的沒有什麼地方吃東西...  

會議配合的團是  
☞ [**Grand Canyon Adventures** **Grand Canyon Guided Tour**](https://www.dothecanyon.com)：$174美金（包含午餐跟點心飲料）  
☞ [Klook上類似的行程從**鳳凰城**出發](https://tinyurl.com/yqsgp3af)：$10,298台幣

從拉斯維加斯出發的選擇就比較多了：  
☞ [**KKday**大峽谷跟團](https://tinyurl.com/ywllz64l)  
☞ [**Klook**大峽谷跟團](https://tinyurl.com/yv3esnxo)

### 大峽谷小攻略
1. **不要餵食野生動物**，因為大家的餵食松鼠變得很有侵略性很危險，導遊稱之為**Tree rat**。
2. 導遊不推薦**騾子行程**，因為**屁股**會很痛。
3. **直升機行程**不能進入大峽谷都只能在邊邊繞。
4. **渡鴉**很多到處都是，行為像八加九。

## 景點路線

大峽谷南緣的光觀路線很廣，  
如果不是跟團可以自己搭**巴士**踩點，（點擊這裡↦ [巴士路線表](https://www.nps.gov/grca/learn/news/upload/sr-pocket-map.pdf)）  

我們走的路線是[**Desert view drive**](https://www.nps.gov/grca/planyourvisit/desert-view-drive.htm)，  
這一條路線是沒有巴士的只能駕車前往，  

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d66722.5871039512!2d-111.97083337741984!3d36.0252311617597!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1szh-TW!2stw!4v1696492346775!5m2!1szh-TW!2stw" width="100%" height="450"></iframe>

### Yapi point
進入大峽谷後第一個點是**Yapi Point**，  
這裡是我自己覺得最有畫面衝擊的點之一，  
一走到懸崖邊整個大峽谷映入眼簾，  
真的很像一塊綠幕的假背景！  

導遊也很貼心的準備一個**望遠鏡**，  
會指一些下方特別的點來介紹，  
例如在這裡我們看了下方的橋、幻影牧場（Phantom Ranch）、泛舟的人們之類的。

這裡入口處有個**小店**，通常人都會很多，  
因為最後Desert Point的紀念品店比較大，  
所以如果確定會去紀念品店的話可以不用進去人擠人，  
當然對於地質學有興趣的還是很推薦進去看看。

<img src="https://img.poibank.com/V8md0X1RTlIrD-ysOgd9XHpZK1o=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvOGI1MGY4NGEtMjRiZC00MTUyLWFjYTgtN2ZmYWFiNDUxNmZlIn0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/>  
<img src="https://img.poibank.com/qAauGdh-ZXTHCd3lWeWXy5GOvOw=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvNGM3Y2UzN2QtMjc4MC00NTExLWFhMTgtZWQ1ZmRhMTA5OTI1In0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/>  

這裡也很好拍照，蠻多遊客會坐在懸崖邊攝影，  
但不是很推薦這麼做，  
因為導遊說每年都很多人**掉下去**。
<img src="https://img.poibank.com/Zo-61JX4VB-TIFWJ60iKVUACKNU=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvOWI4ODljMjAtODU0Ni00MGM3LWI3NTItZjAzMWNmZmEyMzYzIn0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==" width="45%"/> <img src="https://img.poibank.com/e1NuFbc8se4alQ4OgaIzgusdFp8=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvMDc1MjJlNTgtMjlhZS00OTVjLTg0NDgtMGNjZTgxOTk5ZGI2In0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==" width="44.5%" /> 

### Duck on the rock viewpoint

這個點的特色是可以拍墜崖照，  
不過我比較害羞不好意思拍，  
這個點因為我一直忙著看渡鴉，  
所以沒有很認真的在聽導遊介紹～  

<img src="https://img.poibank.com/BKLIcNSuITrEi46bP4h7nuANFEc=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvYzRhYzk3YzgtMWZkNC00NzljLTgzMDktNzVhYWI3MTVlMTJhIn0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/> 

### Moran point
這個點是導遊讓我們午餐的點，  
導遊準備了一人一個**三明治**，有火腿和火雞可以選擇，  
另外還有草莓、沙拉和飲料餅乾。  

因為這個點看得到**科羅拉多河**，  
所以給了另外一種感覺，  
加上天氣變陰了所以也不會熱。

<img src="https://img.poibank.com/HfA_6Pc2jERZ9NePoo9Fv8wqo7k=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvZTQxY2RiYzItMWMyMi00MDk0LWE3ZDUtYWI2MjMxMjYxODUyIn0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==" width="100%"/> 
<img src="https://img.poibank.com/cjhEXlMxOSmzG9kehMl-WedrRHE=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvMTJlNmNhY2MtMWUwMy00NTkzLWFkZmYtOGNjOWVmYWMyNzE3In0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/> 

路上經過看到**馬鹿（Elk）**都不會怕人啊～
<img src="https://img.poibank.com/x0Z5eT3lFyuS_LZC4Lg4u2MR1Tg=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvYTdiZTUxMmQtM2Q5YS00YTBkLTlmMWUtNmFkMWU5MTJmZjc1In0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/> 

### Lipan point
到Lipan Point時可以看到天空都是雲，  
其實還有一堆**閃電**，  
所以導遊說如果看到女生頭髮飄起來（靜電）就要趕快跑回車上，  
畢竟我們在這麼高的地方又沒有遮蔽物真的和人體避雷針一樣。  

比較意外的是我這麼不愛石頭的人，  
每次看到這樣的風景還是很驚艷沒有疲乏，  
在懸崖邊看這些開闊的景色，  
就會覺得整個壓力也開了，都快忘記後天我還要報告呢。

<img src="https://img.poibank.com/dmtJOU7ioN-S9sRkXl94XsjauuE=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvZDI2ZTFiNTUtZTBiYi00YjY4LTg3M2YtN2QyNzI3NDZjM2Q3In0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/> 

### Desert viewpoint
來到**Desrt point**卻狂風暴雨也是蠻搞笑的，  
導遊給我們一個**像垃圾袋的輕便雨衣**就放我們下車然後高歌離席（？）  
有多像垃圾袋？  
無袖有帽子的輕便雨衣！**無袖欸！無袖！**  
看看我在風中蟾蜍的樣子！
<img src="/img/blog/usa-esta/grand_canyon_rain.png" width="100%"/>  

因此一下車我就跑到紀念品店**Desert View Market & Deli**去逛逛躲雨，  
這裡是園區內可以買咖啡和比較多紀念品與生活用品（露營用）的地方，  
我在這裡買了**紀念貼紙和明信片**，  
然後躲到與小一點再出發去瞭望塔。
<img src="https://www.nps.gov/common/uploads/cropped_image/primary/A52E8BA3-EB87-DAC1-E8D5351D00B5488E.jpg?width=1600&quality=90&mode=crop
" width="100%"/> 

> 圖片來源：https://www.nps.gov/places/000/desert-view-market-deli.htm


最後這個點我覺得非常推薦，  
塔的一樓可以進去參觀，但**上塔要買票**，而且人很多，  
因此我只在一樓逛一逛。  
<img src="https://img.poibank.com/QjwyvbrOrSE_MnE9JVQid8XAliY=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvNmRmMWMzNzgtNGMzZC00OTYyLWE4MWItYTg3NWI1MGNhNDA1In0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/> 


一樓外面的風景我會排第一名！  
我的是最美的點，又有河從畫面中間切割，  
視野也非常遼闊，  
**如果只能選一個點去我會選Desert point**！  
<img src="https://img.poibank.com/7tnSFQAo2lDjsDgueEPSLcZRRGs=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvOTc4NTgvOWY1NGQzMDYtZDczNS00MWQwLWEwYmMtOWJjNTY5N2NjZTI3In0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==
" width="100%"/> 

其實逛到這裡也下午四點多了，  
再來我們就準備離開大峽谷了，  
回到車上吃著跟團提供的零食飲料，  
慢慢地晃回弗拉格斯塔夫休息，  
回宿舍準備明天的會議啦~

## 心得
大峽谷是一個蠻值得看看的地方，  
當然我不會說一生必去，  
人生很短世界很大，到處都必去去不完的。

但如果有機會經過大峽谷周圍，  
是蠻建議花個一天半天去逛逛，  
當然對於這種景色特別有興趣的人可能要花個兩三天才夠。

由衷建議，在春天或秋天來**因為真的熱**！


<iframe src="https://youtube.com/embed/CdEuDm0EAmQ?si=ctZSLGHZlMmSyxRS" width="100%" height="450"></iframe>

## ☺ 

:::note 美國旅遊相關

[美國入境相關資料](https://peiyu.us/blog/2023-usa-before)   

:::