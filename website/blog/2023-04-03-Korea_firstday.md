---
slug: 2023-korea-first-day
title: 2023 || 韓國旅遊 | 韓國校服體驗＋石村湖賞櫻
tags: [櫻花,體驗,旅遊, 韓國旅遊,2023]
comments: true 
---

【 2023.04.03 韓國旅遊 第一天 韓國感性校服體驗＋石村湖賞櫻半日遊 】

入境韓國後，  
我們的第一站就是去追提早開的櫻花！  
但單純賞櫻太無趣了，就租了韓國校服以青春的姿態去石村湖追櫻吹雪！

<img src="/img/blog/seoul_day1_0.png" width="100%"/>  

<!--truncate-->

> **第一日的詳細行程可以看**[**funliday**](https://www.funliday.com/berit/journals/80835)！

## 感性校服

放完行李後，我們就搭著地鐵到了**蠶室站**，  
走約5分鐘的路程就會到石村湖和樂天樂園附近，  
但我們要先去**感性校服**租一套情侶學生服來裝嫩！  

原本在梨花校服與感性校服兩者中做選擇，
雖然感性校服的經驗分享資訊比較少，  
但因為這間店比較新，而且棚內長得很可愛，所以最後選擇他了！  

這個體驗我事先就在kkday上面定好了**不指定時間**的方案 ([點擊這裡購買↦](https://tinyurl.com/2mnakqo5))，    
也就是隨時都可以去借（人多時可能要排隊）當天關店前要歸還。  
**價格為一人443元**！

*****
### 交通方式
:::info 感性校服 蠶室本店 감성교복 gamsung gyobok

地址｜서울특별시 송파구 석촌호수로 234 라품미 빌딩 4층  
交通｜ 2號線 蠶室站（3號出口）  
時間｜09:00-22:00（時間可能會隨著樂天世界而變動）
<details color="black">
  <summary><b>點開看地圖</b></summary>
  <div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3165.012529843411!2d127.10010967724567!3d37.507622627501654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x357ca5355d3f23a9%3A0x185d1a88ecd4ba9b!2z7LmY7KaI7JWk7IO37Y-s7Yag67aA7Iqk!5e0!3m2!1szh-TW!2stw!4v1682413850125!5m2!1szh-TW!2stw" width="600" height="450"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
  </div>
</details>

:::
*****

蠶室站出站後走路約**15分**鐘可以到感性校服，
但因為在**4樓**所以不好找，  
可以參考[google map定位](https://maps.app.goo.gl/GY5snG6NXq9RszQv6?g_st=ic)但最好還是認下面**招牌**！
從旁邊的門進入往上走到4樓，  
3樓長得也很像目的地但其實是置物間。

<img src="https://img.poibank.com/YmDD51GEY17FFnXMWGkCPMw_3zI=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNTNiN2I4NzktZDY5My00MTRhLWFlYzAtMjNjYjUxZWRlNjg2In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>  

### 試穿制服 

到樓上後要先把包包放在外面置物櫃（**記得拿錢拿護照**！），  
當天的店員也不太會講英文，  
因此就直接秀給他們看kkday的憑證他們就會懂了，  
接著他們會給你看一些規定包含要幾點歸還等等（有英文版本），  
之後就可以去試衣間選衣服了！

總共可以試穿兩套衣服，  
因為韓國的尺碼跟我們習慣的不太一樣實在不好挑，  
平常我選衣服S-L都有可能就還蠻困擾的...  
但**Ｍ號大約是66尺寸**，所以可以從這個大小開始試起！  

<img src="https://image.kkday.com/v2/image/get/w_960%2Cc_fit%2Cq_55%2Ct_webp/s1.kkday.com/product_123181/20211124053522_T7U8y/jpg" width="100%"/>  

> 圖片來源：KKday


襯衫、裙子/褲子、毛衣、毛衣外套和領帶領結都是不用另外加錢的，
但是**制服外套要另外收5000韓元**，  
如果有需求也可以跟櫃台購買絲襪、襪子、小背心等等商品喔！  

<img src="https://image.kkday.com/v2/image/get/w_960%2Cc_fit%2Cq_55%2Ct_webp/s1.kkday.com/product_123181/20211124053521_pg6JX/jpg" width="100%"/>  

> 圖片來源：KKday


旅伴對窄裙有特殊愛好，  
可惜窄裙樣式比較少，只有灰色可以選，  
因此就搭了一套灰色系制服，  
但是男生灰外套已經沒有尺寸，  
只好穿毛衣來搭一套同校情侶制服！  

<img src="/img/blog/seoul_day1_1.jpeg" width="100%"/>  

這是唯一一張全身合照 哈哈！  
還好選衣服的地方有照片可以參考怎麼搭配，  
不然兩個人會討論到打起來的！

試穿完之後就可以到櫃檯**結帳並簽合約**，
再押一本**護照**在那邊，    
店員會給你們一張號碼貼紙，不要忘記上面的數字喔！  
回來後領取護照跟清點要用的。

可以到3樓寄放衣服和物品，  
置物歸空間都不大，只能放隨身物品，  
一次要投1000韓元（**500元硬幣兩枚**），  
沒有零錢的話建議在櫃檯先和店員兌換！  
3樓還有正在流行的人生四格拍貼機，有興趣的可以在這裡玩一下。

### 棚內空間

感性校服的**棚內**也很好拍，  
還有腳架可以借用非常貼心 :heart:  
尤其是粉紅教室那棚光線很好，  
建議大家可以留點時間在裡面拍照！

<img src="https://img.poibank.com/-BMOiAQslKyQC5BGn4kg1hfx9cc=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZmI0YjM4ZDgtMmIzMS00YjM5LWJiNWQtMDA3YjBkOTQyNTZkIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>  

<img src="https://img.poibank.com/P6675NX6wTs0JFkfLOGUGpEkikM=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYWU4NjdlNzctMTI2My00ZjFmLWIxN2MtNzBjOTJiNmZiYjYwIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="33.3%"/> 
<img src="https://img.poibank.com/k79yUbpLXFMp4TQG3Zf0PYzQV-A=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNmNiNDRiNTktMjhmMy00ZGEzLWFkY2UtODk2ZDIyNzFiNzliIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="33.3%"/>  
<img src="/img/blog/seoul_day1_2.jpeg" width="33.3%"/>  

## 石村湖
走出感性校服過個馬路就會到石村湖，  
石村湖是兩個圓形湖組成的venn diagram，  
我們只走了圍繞著樂天樂園的那一個圓，  
因為這圈比較好拍！

首爾的櫻花今年比預估的櫻花線提早一週，  
石村湖今天已經開始**櫻吹雪**，  
等到石村湖的櫻花季活動開始應該都掉光光了。

有櫻花就是很好拍，  
加上我的攝影師技術很好，  
怎麼拍都好看！  
遠遠的也可以看到樂天塔，  
還可以聽到中間樂天樂園不間斷的尖叫聲，  
算是蠻愜意舒服的漫步旅程。

<img src="https://img.poibank.com/KUvRxykTgbm06m1XvUycYy83jps=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvODg0NDZjOGYtMjJkOS00YjhiLWJmMDgtZjQ4ZGFiMWU5MjQyIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="33.3%"/>  
<img src="https://img.poibank.com/N-oYe1UDGhhFQeK9qY_k9dmD8HY=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZjdmMjA2ZTQtODlkYS00MmFhLTg4MzktNDRmMmM2M2YzZWM4In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="33.3%"/> 
<img src="https://img.poibank.com/x0Mg5BNUiiMOjYQROjfkb9-4Zc8=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYWE4YTczMjktZmZiZS00YjI1LWFkMjgtNDk0ZDY5YWIzZWE4In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="33.3%"/>  

<img src="https://img.poibank.com/aa20gVl-CQqzmVULNh6BQCnmJTM=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMTdlOGU2MGItYjBmMy00OTBmLWE4NzEtZDc1OWZhMDk0MjBmIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>  

因為下飛機後就沒有進食，中間肚子太餓了，  
就跑去旁邊的**MEGA COFFEE**吃點點心，  
查了之後才知道原來是韓國版85度Ｃ，  
東西不貴吃起來還可以，室外座位區還可以看到樂天塔。

<img src="/img/blog/seoul_day1_3.png"/> 

## Craft Island
晚餐和旅伴的韓國親咕見面吃晚餐，  
他選了一間像是韓劇裡面上班族下班聚餐的那種餐酒館，  
食物是沒什麼特別的啦，  
豬腳三個人分食所以沒有吃飽，
回飯店前又跑去emart買了牛奶。


<img src="https://img.poibank.com/vnuKOrccAmc5GBiAo34UhhveZhE=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvODNiMGVhYTItMDM1ZC00MzM0LTg4MGMtNjM2ZWVjOTUwNmYzIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>  
<img src="https://img.poibank.com/Izwet9VuAqyRFz6W8zXXHNLZE1Q=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZWUzZDE5ZGEtMjQxMi00ZDQ4LTgxMzItZWQzODVjOTJiNmJkIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>  

## ☺ 第一天總結
今天天氣好，  
入境跟一切都很順利，  
是一個美好的開始 :heart:

:::note 韓國旅遊相關

[北村韓屋村](https://peiyu.us/blog/2023-korea-last-day)   
[韓服體驗＋景福宮](https://peiyu.us/blog/2023-korea-third-day)  
[韓式證件照＋南山首爾塔](https://peiyu.us/blog/2023-korea-second-day)  
[韓國行前住宿交通換錢等須知](https://peiyu.us/blog/2023-korea-preparation)  
[韓國入境相關資料](https://peiyu.us/blog/2023-korea-keta-qcode)  
[韓國WOWPASS申請教學](https://peiyu.us/blog/2023-korea-wowpass)  

:::