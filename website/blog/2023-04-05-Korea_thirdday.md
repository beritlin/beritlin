---
slug: 2023-korea-third-day
title: 2023 || 韓國旅遊 | 韓服體驗＋穿越景福宮
tags: [美食,體驗,旅遊, 韓國旅遊,2023]
comments: true 
---

【 2023.04.05 韓國旅遊 第三天 雨天景福宮拍照守則 】

心心念念的韓服體驗竟然遇到**大雨**，  
雖然很失望，但還是拍出不一樣味道的照片啦。

<img src="/img/blog/seoul-day3-0.png" width="100%"/>  

<!--truncate-->

> **第三日的詳細行程可以看**[**funliday**](https://www.funliday.com/berit/journals/80835)！

## Issac
第二個早晨換一間早餐店，  
東大門Issac比較晚開店（8:30），  
我27分就到門口當第一位客人，  

和Egg Drop都是年輕店員不一樣，  
Issac都是阿珠瑪在準備餐點，而且中文比英文好用！

<img src="https://img.poibank.com/l7m-CGzQSmR9K8UmTdVDbFKkkJY=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMzQxNThlNzYtZWQwOS00MjJkLWEzZWEtMGRiYmI4NzkyMzBkIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"  /> 
<img src="https://img.poibank.com/-LvIHJT3HztR4wyqDYsxsbbIBpw=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMDA3MjliYTAtNjA1Mi00NzdjLWFhODAtODBiOTc0ZGY3MWNiIn0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==" width="50%"  /> 


## 西花韓服
來之前聽說太晚來會沒漂亮衣服選，  
原本想要一開店9:30就來結果**KKday**上被預約完了，  
趕快約一波10點的 [(點擊這裡購買↦)](https://tinyurl.com/2lwad5hx)，  
出發前幾天我還一直做惡夢夢到我沒有漂亮韓服穿！

西花韓服都有**中文**接待，
完全不用擔心溝通的部分，
女生衣服選擇很多，男生少的可憐哈哈！  
一個人可以試兩套衣服，  
最後選了一套粉粉嫩嫩的韓服，
有**襯裙**蓬蓬的超級加分，  
捷哥自己挑了一套世子的服裝。

<img src="https://img.poibank.com/Q7PV-md2KxWmlBXuwAq9DxZWmo4=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNDg5Y2FjZTItNzI0NC00MzE5LWI2MjQtZmU3YWFhMDBjNjVjIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"  /> 
<img src="https://img.poibank.com/5KAYYdbqwOGozEuiGS6_YP0_cEI=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZDA2NDIyNjktMjQzZC00NTk0LWE0ODItZjFiY2NkZTJkNmM5In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 


**編髮**服務就普普啦，
他們就抱持著有幫你編就好這樣去編，
但包包髮夾幾乎都可以**免費**外借，
算是不錯👌🏻

<img src="https://img.poibank.com/P6knhxsvjNKzqlEwYArF8eHW6wQ=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvODE5MzJkMDgtZjMzNC00YWJmLWI3MmQtM2IxNDhkZWYwYTNjIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 
<img src="https://img.poibank.com/cBfrvLFobntgaMiMCUOOuc69G_U=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNWIwMDI4NTctNTI5Mi00MmFiLTkxZTgtNjdjNWYyOGI4Y2E0In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

弄完之後把要寄放的東西給櫃檯，  
押一本**護照**後就可以出去玩了～
從kkday上訂行程可以多穿兩個小時喔（共**六小時**）！

## 景福宮

西花韓服對面過馬路就是景福宮，  
走個五分鐘會到正門口，  
門口會有**守門將**超酷的！  
以前我都以為他們是假人，  
結果竟然是真的！

10:00、13:00、15:00他們會有**交接儀式**可以看，  
可惜我們錯過了第一次交接。

<img src="https://img.poibank.com/0dVVDnOVeV2g8M979pD0eKjPANw=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYTM2NTQ1NzYtMTRmOC00NDliLWIyYWQtYjdiOGJhYjJkOThkIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 

進入之後就可以看到一堆遊客！  
右邊可以買票，但因為穿韓服就免費，  
所以只要直直的**往門口衝**去就對了！

雖然下雨，  
但是只要在屋簷對好視角，  
還是可以跟**勤政殿**拍照片～

<img src="https://img.poibank.com/ez7XhcokG294Sgny7ebO2IX1cxI=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvODlhYTVkNzktOTJkZC00ZTQzLTlkN2YtYWFmOWZmYTU3NjJiIn0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==" width="100%"/> 
<img src="https://img.poibank.com/PjmmtjkAhaDHwikLyokRpb0HlPo=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNGUxMGYyZWItMzJmYy00YWUzLWIzMGYtOWNkZmM4Yzk5ZjIyIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>
<img src="https://img.poibank.com/IINP2i0_ExZWXoHN-HDMR1SYoZQ=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYjQwMjE5OTMtNWMzYi00ZTNmLWIwYWItNmM3MDJjNTc1YjE3In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>

兩旁也都有**廊道**可以走，  
不只可以拍照也可以避免淋到雨，  
靠近門口的廊道會很多拍照，  
可以直接跳過，因為後面還有數不完的廊道可以拍！

<img src="https://img.poibank.com/19xVs2MGm9BAiHNdRdm_yoUne7E=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMDViMjM5NzQtMWYwNi00MDFlLWE0MjgtNjAyNjc4YzIxNTZmIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>
<img src="https://img.poibank.com/BjgCWEqdke7cny01f7y0OYVxDjU=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZTM4NTE2OGUtZmVkOC00NjhhLTlmNmMtNDA5YzM3OWNlYWY2In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>
<img src="https://img.poibank.com/ep61bsNSrOeXdGGZpg-JAGm-fNc=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNmI1MmUxOWQtM2Q0Zi00YjMyLWFjMWQtYzhlOTc5OTNhMDRmIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>

簡單拍一下就回去回衣服了，  
因為雨天實在是太難好好逛這個諾大的宮殿，  
實在是蠻可惜的！

## 羅州牛骨湯
結束就去旁邊巷子的**羅州牛骨湯**吃一碗熱呼呼的熱湯暖身子了～

<img src="https://img.poibank.com/KBrFsM4pp4M7SEs3gusWtd_QN7M=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZDFhYzNjNmQtYmM0NC00NWQzLWFiOTQtZjIxOWI2MzIxOWJiIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>


原本下午要順路去三清洞和北村韓屋村逛，  
但下雨只好取消，改回明洞和東大門逛百貨！

## 哈南豬
晚餐到飯店樓下的哈南豬繼續吃燒烤！  
<img src="https://img.poibank.com/tL2n_gusjc5zbmOjzdqD5gMOYqs=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZjExNDExNWEtNDQ4Mi00Njk0LTk1MWUtZTZjZjUzYWY5ZGQ2In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>
<img src="https://img.poibank.com/flHQXpTX9HLzzV89LnkFgVf_gT8=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMmEwOTg2MDAtYjhiOC00YWVjLWE2ZjMtMWQ1MzEyNjhlY2RiIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/>


## ☺ 第三天總結
雨天好掃興啊 

:::note 韓國旅遊相關

[北村韓屋村](https://peiyu.us/blog/2023-korea-last-day)   
[韓式證件照＋南山首爾塔](https://peiyu.us/blog/2023-korea-second-day)  
[韓國校服體驗＋石村湖賞櫻](https://peiyu.us/blog/2023-korea-first-day)  
[韓國行前住宿交通換錢等須知](https://peiyu.us/blog/2023-korea-preparation)  
[韓國入境相關資料](https://peiyu.us/blog/2023-korea-keta-qcode)  
[韓國WOWPASS申請教學](https://peiyu.us/blog/2023-korea-wowpass)  

:::