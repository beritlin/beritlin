---
slug: 2023-korea-wowpass
title: 2023 || 韓國旅遊 | WOWPASS辦卡教學
tags: [Wowpass, Tmoney, Card, Trips, Seoul, Korea, 韓國旅遊,2023]
comments: true 
---

【 2023 韓國旅遊 WOWPASS辦卡教學＋回饋碼分享 】

出發前要換韓元時，發現大多數人傾向到韓國當地私人換錢所換錢，  
但我又覺得要特別過去很麻煩，  
就發現了有這個**非常方便的WOWPASS**，  
可以當**金融卡**、**Tmoney交通卡**和**換匯**！  
匯率也很好，用邀請碼還可以多**0.5%**現金回饋喔！【**邀請碼：G3UYKLDN**】
<img src="/img/blog/wowpass.png" width="100%"/>  

<!--truncate-->

WOWPASS機台在首爾很多**地鐵站**都有設置，[**地圖這裡看↦**](https://www.wowexchange.net/exchange/location/list?viewTp=02)  
除了首爾站我住的東大門站、東大歷史公園站也都有幾台！  
現在連**仁川機場**裡面也有了（在AREX直達車站**站內**），  
因此我這次出發韓國一毛韓元都沒有～  
只帶了10,000台幣，  
主要是怕市場等地方不能刷卡所以要換一些現金準備的。

## WOWPASS辦卡教學
建議可以在台灣就下載好APP並註冊會員【**邀請碼：G3UYKLDN**】，  
等拿到卡片再註冊卡片！
<img src="/img/blog/wowpass17.png" width="50%"/>  

我入境韓國後用KKday買了**AREX機場直達地鐵**，(線上購買更便宜！ [點擊這裡購買↦](https://tinyurl.com/27f96s5c))  
所以都還不需要花到韓元，打算在站內再買，  
結果排隊太多人只好等去首爾站再來處理了。

### 註冊新卡
首爾站我到了**樂天超市裡面**的機台，  
機台在門口進來左手邊！  
橘色機台下方有放護照的地方、放錢的、插卡的和最下面是領卡領錢的出口。
<img src="/img/blog/wowpass18.png" width="60%"/> 

因為有繁體中文，所以操作起來很容易！
<img src="/img/blog/wowpass2.png" width="50%"/>  

➊ 要開卡的話選擇**WOWPASS卡**服務，要**換錢**也建議選這個！  
儲值在提領的匯率會比下面**購買韓幣**的匯率好喔！
<img src="/img/blog/wowpass3.png" width="50%"/>  
<img src="/img/blog/wowpass4.png" width="50%"/>   

➋ 再來選擇**發行新卡**，並選擇要兌換的貨幣。  
<img src="/img/blog/wowpass5.png" width="50%"/> 
<img src="/img/blog/wowpass6.png" width="50%"/>  

➌ 按照指示放上**護照掃描**，並同意使用條款。
<img src="/img/blog/wowpass7.png" width="50%"/>  
<img src="/img/blog/wowpass8.png" width="50%"/>  

➍ 閱讀注意事項後，就要放入錢錢並等卡出來。
第一次放入的錢會被**扣掉購買卡的錢5,000韓元**喔！
<img src="/img/blog/wowpass9.png" width="50%"/> 
<img src="/img/blog/wowpass10.png" width="50%"/>  

### 換韓元現金
➎ 如果只是要儲值就可以選擇發卡就好，  
想要提領出現金的話就按**提領＋發卡**的選項，並選擇提領金額。
<img src="/img/blog/wowpass11.png" width="50%"/>  
<img src="/img/blog/wowpass12.png" width="50%"/>  

➏ 最後錢錢就會出來啦！ :coin::coin::coin:
<img src="/img/blog/wowpass13.png" width="50%"/> 

### APP註冊
下載並註冊好APP之後，  
記得輸入**邀請碼【G3UYKLDN】**才有儲值優惠喔！  
就可以按**註冊卡片**，用手機掃描就可以了，  
之後通過手機就可以即時知道刷了多少錢，還會幫忙**換算成台幣**喔！
<img src="/img/blog/wowpass1.png" width="50%"/>  

## Tmoney儲值
要使用WOWPASS裡的Tmoney交通卡的話要去地**鐵站的機器**儲值才可以喔！
<img src="/img/blog/wowpass14.png" width="50%"/>  

機器也有中文所以也是很好操作，卡放上去然後放錢儲值就對了！  
因為大部分的機台都要用現金儲值，所以領一些在身上還是比較方便一點。
<img src="/img/blog/wowpass15.png" width="50%"/>  
<img src="/img/blog/wowpass16.png" width="50%"/> 

有這個就不怕刷卡刷不過或是跑來跑去換錢啦！

:::note 韓國旅遊相關

[韓國行前住宿交通換錢等須知](https://peiyu.us/blog/2023-korea-preparation)  
[韓國入境相關資料](https://peiyu.us/blog/2023-korea-keta-qcode)

:::