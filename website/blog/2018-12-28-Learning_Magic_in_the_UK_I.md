---
slug: Learning-Magic-in-the-UK-I
title: 2018 || 英國旅遊 | 哈利波特景點搜集
tags: [Trips, Magic, Harry Potter, London, England, UK, 英國旅遊,2018]
---

這其實是我來英國的真正目的
媽媽表示：「你不要浪費我的錢！」
![](https://media.giphy.com/media/m0Z1LPu6flQDS/giphy.gif)
<!--truncate-->
在英國 尤其是倫敦
就有很多地方是哈利波特曾經取景的地點
當然跟電影中看起來還是不太一樣～

我最想去的哈利波特影城 （Warner Bros. Studio）
不過沒三個月前訂票是不行的
只能含淚等待下次機會

![](https://media.giphy.com/media/3o7529o2bwGupeaaoo/giphy.gif)
另外比較有名的地點就是國王十字車站 King’s Cross Station的九又四分之三月台 Platform Nine and Three Quarters
一到車站馬上就有要前往魔法世界的感覺 （開始唱起BGM）
這裡是麻瓜世界和魔法世界連接的橋樑啊！
![](https://media.giphy.com/media/11gQoowuX9XrYQ/giphy.gif)
當時去的時候是假期間 又是人最多的午後時段
所有人都在排隊穿越月台
整整排了一個多小時才拍到照片
不過當然是值得的（迷妹）
![](https://lh3.googleusercontent.com/TQZbzgb4zUOE_gauVFPq_jBTghXfdL2lThkXsYYF9Vpy5e4qZNw0eZSFzeGBxnPgJJaNsDAkU4VjqTTejuMhpNqpC5qs1KRxoQkPVLsx0m8cJryoSu7SIAe2fnAuqwVR8c6M8dIwQA=w2400)
首先可以先選擇你的學院圍巾
大概90%的人是選葛來分多（Gryffindor） 捷哥也是
再來是些許的史萊哲林（Slytherin）
然後是跟我一樣身為赫夫帕夫（Hufflepuff）的學生
跟乏人問津既無主角也無配角光環的勞文克勞（Ravenclaw）好慘

總之再來就必須就定位 後方有個麻瓜工作人員幫你甩圍巾
前方有另一個麻瓜工作人員幫你拍照
如果要買他手上的那張照片加上霍格華滋入學證書要15鎊

![](https://media.giphy.com/media/9NyM4FWtm1rws/giphy.gif)
之後可以進去商店逛逛買紀念品
我帶了一隻玻璃獸（Niffler）回來
希望靠他的天性讓我一輩子(混吃等死)享受美好人生

![](https://media.giphy.com/media/AV5y1BxRNEATK/giphy.gif)
![](https://lh3.googleusercontent.com/AitHZsCYx82l8v3eZoYGTxzeh5SmGC7AbJ_05I3IfGsJfIC5Bn2vMVto7R2Hcw7NqhPERn-gZGOTzkJU3xnTWqNi90MLN_yQ8sgzJwiSFAe91hSYEZ7rsRuL6VQvyw5qxeyr_dFJ7A=w2400)
在國王十字車站隔壁就是聖潘克拉斯車站 St Pancras International
出現在哈利波特第二集
也不知道榮恩在想什麼
原來是在這麼熱鬧的地方開飛車

![](https://lh3.googleusercontent.com/lu11_2xs2oSVasuDTEL2sALE2C3Ss-Yx8VtnlnPC7dQq9T3x4Kk8oI9D9CDsl_OegA_Mi-YSU3w2zbxhYkKRMkYiGnklXMh5thl_vtYMaWiuesrez4LKo9cMi50-X49Zf5d8Cal0Kw=w2400)
這隔壁的車站也是很壯觀
帶著我剛買的玻璃獸踏上哈利波特踩點之旅

![](https://media.giphy.com/media/4yjbPjLBSj0n6/giphy.gif)
另一個倫敦的鬧區皮卡迪利圓環 Piccadilly Circus
則是出現在哈利波特第七集

![](https://lh3.googleusercontent.com/IFk-EvW4okcskkQkewvZe7d8IDikdJPiNvXZBtnbXScIgxZylEt3UmazOgjoocNe9yw7PECw1HVO6NpBVCNsWQ11r_ZpdNik5WAYV3W4u1Fotf2TgZEWY9sw_nDxFwK2OLn5vBQoCw=w2400)
不過我們路過沒有拍照
因為趕著去下一個廣場追舊式巴士
所以就用塔拉法加廣場做代替XD

![](https://media.giphy.com/media/pOQE2BWLAYH7y/giphy.gif)
還有前幾篇介紹到的[倫敦動物園 ZSL London Zoo](https://peiyu.us/blog/2019/01/09/ZSL%20London%20Zoo)
但由於動物園四點就關了
中的爬蟲館是哈利波特第一集
哈利第一次說爬說語時的場景
可以順便看看上一篇

最後
我還有好多景點沒有看到
不過只要還在英國
我就會繼續在學習魔法之於 好好找尋這些景點的～～～

