---
slug: 2023-korea-keta-qcode
title: 2023 || 韓國旅遊 | K-ETA申請／Q-Code申請 
tags: [k-eta, q-code, visa, Trips, Seoul, Korea, 韓國旅遊,2023]
---

【 2023 韓國旅遊 行前準備 II 】

:::danger Note
**4月1日**起台灣進入韓國不需要申請K-ETA！  
**7月15日**起台灣進入韓國也不須申請Q-code！  
這篇依舊留著紀念這慌亂的一年吧～
:::

入境韓國前，有些該做的事情還是要先完成，  
才能避免行前慌亂，又可以省下一些些入境時間，  
提早開始玩耍！

因此我們開始各方面行前準備吧～

<img src="/img/blog/keta0.png" width="100%"/>

<!--truncate-->

從2022年11月起韓國開放台灣人免簽入境，  
只要在飛機起飛前72小時**之前**申請電子旅行許可**K-ETA**，  
和在飛機起飛前72小時**之內**填寫入境檢疫資訊**Q-Code**，  
就可以準備出發韓國啦！

## K-ETA申請步驟
再複習一次，K-ETA（ Korea Electronic Travel Authorization）是出發前72小時**之前**就要申請好的，  
費用總共是10,300韓圓，大約250台幣，  
因為審核時間最長可能需要三天，所以請記得提早申請喔！  
申請一次K-ETA的期限是2年，但是**如果期限內換了護照一樣要重新申請一次**！

K-ETA可以在[**官網**](https://www.k-eta.go.kr/portal/apply/index.do)申請也可以用**app**申請，  
如果用電腦要注意到底是不是官網，還是詐騙的代辦網站！

但因為我覺得用app拍攝資料比上傳資料方便，因此我選擇使用app來進行，  
只要下載官方K-ETA的app就可以！  
這裡教你**10分鐘完成K-ETA申請**：
<img src="/img/blog/keta1.png" width="50%"/>

進入app之後選擇**左邊**的申請簽證，
因為語言沒有中文可選，所以以下都以英文作為說明。
<img src="/img/blog/keta2.png" width="50%"/>

➊ 第一頁要選擇所在地區及國家，台灣人就選擇**亞太地區**和**台灣**，  
下面都是個人資料索取使用的同意書，只要全部案**同意**就可以進入下一步驟。
<img src="/img/blog/keta3.png" width="50%"/><img src="/img/blog/keta4.png" width="50%"/>

➋ 第二頁為聯絡資料填寫，先輸入**護照號碼**，接著輸入**常用電子信箱**就可以，  
申請好的K-ETA會寄到填寫的信箱，所以建議不要亂填喔！
<img src="/img/blog/keta5.png" width="50%"/>

➌ 第三頁要填寫護照資料，可以用點選 `Add file` 來拍攝護照，就可以自動上傳以下資料會比較方便，  
<img src="/img/blog/keta6.png" width="50%"/>

不管資料用掃描還是手動填寫都要記得再檢查一次喔！
<img src="/img/blog/keta7.png" width="50%"/><img src="/img/blog/keta8.png" width="50%"/>

➍ 第四頁要填寫一些申請資料，從上而下為：是否有雙重國籍、台灣的聯絡電話、是否以前有去過韓國以及到訪韓國的目的，  
這裡我是選擇**旅遊**，會接著出現選項讓你選擇是以**自由行**還是**旅行團**的方式旅遊。  
<img src="/img/blog/keta9.png" width="50%"/>

再來會請填入在韓國到訪期間的**居住地址**以及**聯絡電話**，  
我就到booking.com上面複製我的的**飯店**的地址和電話輸入就可以，  
最後則要你選擇你的**職業**，我選了一個最通用的**上班族**。
<img src="/img/blog/keta10.png" width="50%"/>

接著請你選擇最近是否有生病，通常選**沒有**就可以。
<img src="/img/blog/keta11.png" width="50%"/>

最後會要你**上傳大頭照**，因為用手機的關係會直接打開前置鏡頭，  
只要在背景不要太亂的地方拍一下能**清楚辨認是本人**的大頭照就可以，  
原本申請的時隨手亂拍怕不會過，沒想到也順順利利通過了，哈！
<img src="/img/blog/keta12.png" width="50%"/><img src="/img/blog/keta13.png" width="50%"/>

➎ 第五頁就會請你**確認資料** （果然又被中國了哭），就可以準備付錢了！
<img src="/img/blog/keta14.png" width="50%"/>

➏ 付錢頁面先勾選所有選項，然後在輸入**信用卡資訊**就可以了，  
我用台新的flygo卡和國泰的cube卡都很順利的刷過了！  
建議幣值直接選擇**韓圓(KRW)**，通常比較划算。
費用是韓圓 ₩10000（申請費用）+ ₩300（手續費）＝ **₩10300**，  
我刷完是台幣**$242**元。
<img src="/img/blog/keta15.png" width="50%"/>

➐ 最後就會跟你說**已送出申請**，同時信箱也收到一封**申請完成**的信件。
<img src="/img/blog/keta16.png" width="50%"/>
<img src="/img/blog/keta17.png" width="50%"/>

➑ 如果想查詢**申請進度**，也可以點選首頁**右邊**前往查詢頁面，  
只要輸入**護照號碼**和**出生年月日**就可以查到資料，  
申請進度就會顯示在最下面，例如我當時查詢顯示簽證還在**審核中**。
<img src="/img/blog/keta2.png" width="50%"/>
<img src="/img/blog/keta18.png" width="50%"/>

我申請後等**不到一個小時**就都有收到已經**許可**的信件，  
所以速度還是很快的！甚至是我晚上9點多申請也一下就好了，  
難不成韓國公務員晚上加班嗎？
<img src="/img/blog/keta19.png" width="50%"/>

## Q-Code申請步驟
再複習一次，Q-Code是出發前72小時**之內**才能申請的喔！

直接到[**Q-Code的官訪網頁**](https://cov19ent.kdca.go.kr/cpassportal/biz/beffatstmnt/main.do?lang=en)，  
首頁右上角可以選擇**中文**，雖然是簡體字但還算清楚，  
就可以點選中間按鈕開始填寫入境檢疫資訊，
<img src="/img/blog/qcode13.png" width="50%"/><img src="/img/blog/qcode12.png" width="50%"/>

➊ 第一頁只要選擇**年齡**是摟超過14歲和**勾選**下面資料使用條款，  
就可以進入下一步。
<img src="/img/blog/qcode11.png" width="50%"/>

➋ 第二頁要填寫**護照號碼**和**電子郵件**。
<img src="/img/blog/qcode10.png" width="50%"/><img src="/img/blog/qcode9.png" width="50%"/>

➌ 第三頁要填寫個人資料。
<img src="/img/blog/qcode8.png" width="50%"/><img src="/img/blog/qcode7.png" width="50%"/>

➍ 第四頁要填寫入境後的資訊，**地址**的部分一樣填寫飯店地址，建議用郵遞區號選擇比較快。  
<img src="/img/blog/qcode6.png" width="50%"/><img src="/img/blog/qcode5.png" width="50%"/>

電話的部分也是一樣填寫飯店的聯絡電話就可以！
<img src="/img/blog/qcode4.png" width="50%"/>

➎ 第五頁只要填寫最近停留的國家，我最近只有在台灣所以選擇**台灣**就可以，  
然後會請你勾選是否有不舒服的症狀，一般來說選**無症狀**入境應該都沒問題。
<img src="/img/blog/qcode3.png" width="50%"/><img src="/img/blog/qcode2.png" width="50%"/>

➏ 最後你的Q-Code就會生成啦！  
記得截圖下載或是印出來，過海關時就可以拿出來用了！
<img src="/img/blog/qcode1.png" width="50%"/>

準備好這些文件，就可以放心的出遊啦 :heart:

:::note 韓國旅遊相關

[韓國行前住宿交通換錢等須知](https://peiyu.us/blog/2023-korea-preparation)  
[韓國WOWPASS申請教學](https://peiyu.us/blog/2023-korea-wowpass)

:::