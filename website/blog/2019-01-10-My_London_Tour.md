---
slug: My-London-Tour
title: 2019 || 英國旅遊 | 倫敦遊記
tags: [Trips, RHUL, Windsor, Staines, Slough, London, England, UK, 英國旅遊,2019]
---

【2018.12.05 – 2019.01.10】

一到聖誕假期 交出作業後就立馬飛到倫敦找捷哥
不過出發那天蠻不幸運的遇到班機延誤（兩小時）
加上因為我為了省錢 沒有買行李額度
我就只能提一小箱行李 只好把所有衣物穿在身上
那天像洋蔥一樣一層又一層的快在寒冬中中暑
總之最後還是順利的到達希斯洛機場
搭了台幣1200元的20分鐘Uber到宿舍（貴到哭）
<!--truncate-->
# Ehgam
![](https://linberit.files.wordpress.com/2019/02/img_4816.jpg)
![](https://linberit.files.wordpress.com/2019/02/img_4814.jpg)
到了這裡 就必須去參觀人家的學校 皇家哈洛威學院
和亞伯丁相比 這裡相對新很多、小一點
但風格和亞伯丁真的差很多
擁有很經典的英式紅磚建築 還是很有英國學校的氣息

# Windsor
![](https://linberit.files.wordpress.com/2019/02/img_4742.jpg)
![](https://linberit.files.wordpress.com/2019/02/img_4743.jpg)
![](https://linberit.files.wordpress.com/2019/02/img_4740.jpg)
離這裡最鄰近的觀光景點就是溫莎城堡了
走在街上就可以感覺到來自世界各地的遊客好多啊
加上聖誕節要到了 街上到處都是裝飾和聖誕樹


跨年前還有特殊活動 城堡周圍排了好多遊客等著進去
偶而還有機會可以看到馬車在前面繞圈

來來回回溫莎也去了好幾次
很可惜這次都沒有進去城堡裡面參觀 

# Staines
![](https://linberit.files.wordpress.com/2019/02/img_4845.jpg)
除了溫莎 我們也去了幾次斯坦斯Staines
這裡全名叫做泰晤士河畔的斯坦斯Staines-upon-Thames
大部分都是當地人在逛的地方
但因為就在泰晤士河旁
只要走在沿岸邊 就真的很美 不知道這裡的房價會多高

# Slough
![](https://linberit.files.wordpress.com/2019/02/img_5517.jpg)
![](https://linberit.files.wordpress.com/2019/02/img_5522.jpg)
另一個當地人常去的地方就是斯勞Slough了
這個地方就沒有光觀客 都是本地人在逛的
因此風景就普普了 但是生活機能感覺不錯
第一次去是平安夜那天去溜冰
在英國第一次溜冰 很慶幸沒跌倒 雖然溜得有點慢
第二次去是去看電影
也是在英國第一次去看電影 這間電影院有多爛就不好說了
價格大概跟台北市差不多 但是整個設備真的是太差了

由於這裡到倫敦市區其實有點麻煩
必須搭一段火車到地鐵站到想去的地方
因為這段路又久又貴 所以每次到倫敦市區就要逛多一點才划算
加上這條火車線又常罷工

# London
## British Library
![](https://linberit.files.wordpress.com/2019/02/img_5585.jpg)
![](https://linberit.files.wordpress.com/2019/02/img_5618.jpg)
第一次出遠門是從國王十字車站一路晃到滑鐵盧車站
從車站出來就可以到旁邊大英圖書館British Library
是英國的國家圖書館 也是世界上最大的學術圖書館之一
裡面真的可以感受到大家拼命的氣氛
位子都坐滿滿 所有人都埋頭苦幹唸書
如果在裡面寫論文感覺就會特別努力
不過這裡館藏的圖書就必須要有會員才能進去看
一般人只能在外面參觀晃晃

## British Museum
![](https://linberit.files.wordpress.com/2019/02/img_5620.jpg)
![](https://linberit.files.wordpress.com/2019/02/img_5576.jpg)
之後一路走到另一個大英系列 大英博物館British Museum
身為世界上最大、最有名的圖書館
進去是不用錢的！
畢竟裡面的東西大部分都是從別的國家來的
只是要排隊排一陣子 我們大概排了半小時才進到博物館裡面
裡面館藏真的很豐富 從我唯一有興趣的自然歷史標本到我完全沒興趣的各國文物
應有盡有 看得真的讓人眼花撩亂
如果是真的對於博物館裡面館藏有興趣的人
建議安排一整天的時間在裡面並且必須要租一台導覽機
不然就只能跟我一樣走馬看花
因為裡面真的很大 沒在開玩笑的大
加上走半天的路了 我根本沒心情好好看這些東西有點可惜

## Trafalgar Square
![](https://linberit.files.wordpress.com/2019/02/img_5596.jpg)
![](https://linberit.files.wordpress.com/2019/02/img_5619.jpg)
離開博物館後 我們就去追英國雙層古董巴士
現在只有兩條線的巴士還有古董巴士在運作
為了追15號古董巴士
我們到了特拉法加廣場Trafalgar Square
就在那裡等公車來 （15號巴士不是每台都古董巴士 要碰運氣
還好守株待兔了十幾分鐘就看到一台古董巴士晃過來了
原本以為要搭不上了
結果任性的倫敦司機把公車停在路邊人不知道幹嘛去了
（這裡的司機常這樣 原本裡面的乘客只能下來搭別班）
反而給我們良好的機會來跟公車拍照
這種從後門上車的舊公車 跟哈利波特裡面的同款啊～

![](https://linberit.files.wordpress.com/2019/02/img_5595.jpg)
雖然我們一天一路在奔波
但其實走路時一點也不無聊
因為倫敦到處都真的很熱鬧
加上聖誕節到了 到處都很多人在購物逛街
還好不是跟我當初上網查的一樣說過年時倫敦是空城的景象

## London Eye
![](https://linberit.files.wordpress.com/2019/02/img_5600.jpg)
後來我們又一路沒計畫的逛 逛到倫敦眼London Eye
雖然沒有坐到摩天輪啦～
不過剛好下方有一個小小的聖誕市集可以逛一下
可惜當時沒有買跨年煙火票 （兩個懶人）
不然跨年到這邊看倫敦眼的煙火應該很漂亮
不過話雖如此 跨年那天我們那的火車又罷工了呢…


![](https://lh3.googleusercontent.com/WEJih1wOQTJPiuLmSQu5uNt-sG163mIN_vdCFHbEPj9SPEWJ2dJY6xwQbwBTiZLqPVkKyAgXANWBUw0pMBvwwWk2L874DeYmDj4YpXSmTq2776N9ZdI6TfJ66egqDTf_RbNr8ulc0Q=w2400)
![](https://lh3.googleusercontent.com/BpGp5cbK0LgIbtISF6GTg6z6SZgRXsrmwjBF7JJ3qFeSTk_RQ4SMtjiH-Am6dMfDOnrqT9iFI9Rh9eqRD-sXZIOMAnCgn8_VLu65akirZwoBXU2J1R7csQa3AXn2yZ3Ecg0unHC5QA=w2400)
第二次入城其實就是到[動物園那次](https://peiyu.us/blog/2019/01/09/ZSL%20London%20Zoo)
但由於動物園四點就關了
剩下的時間我們到了旁邊的肯頓市集Camden Market
這裡是個很大的市集 不管吃的還是衣服首飾紀念品等等都有非常多選擇
感覺有種逛西門町那種感覺啦

逛完市集想說再坐地鐵到牛津街 Oxford Street 逛逛
這裡算是走比較高檔路線了
整條路從平價服飾品牌到精品都有
然後很明顯的已經累到不想拍照了
不過這邊的餐廳通常水準比較穩定
但相對的價格也是高了不少
晚餐吃了日式料理 又一次貴到哭
但是是真的好吃 在這裡要吃到好吃的生魚片或是壽司都很難啊

出來過幾次後 倫敦複雜的鐵路地鐵系統我也摸熟了
真的是比台北捷運複雜一百倍
能征服這裡的交通系統太有成就感了
訣竅就是下載Trainline這個APP到手機 XD
不管是怎麼搭車 車幾點到在哪個月台 有沒有罷工
上面資訊都很清楚 是倫敦必備
我沒有買牡蠣卡 但是都是買13鎊的一日卷（鐵路地鐵公車通用的）
火車的部分我有買16-25的青年卡 因為這裡火車太貴 沒道理的貴
辦了之後其實搭過一次就划得來了

然後不得不說進城一次好累啊…
![](https://linberit.files.wordpress.com/2019/02/img_5720.jpg)