---
slug: 2023-korea-second-day
title: 2023 || 韓國旅遊 | 韓式證件照＋南山首爾塔
tags: [美食,體驗,旅遊, 韓國旅遊,2023]
comments: true 
---

【 2023.04.04 韓國旅遊 第二天 我們的洞內韓式證件照＋廣藏市場買棉被＋南山首爾塔＋王妃家 】

今天的重頭戲是要去體驗一下全套服務的**韓式證件照**，  
去廣藏市場買我此行必買的**棉被**，  
還有去沒有特別想去但好像遊客必去的**南山首爾塔**看風景！

<img src="/img/blog/seoul-day2-0.png" width="100%"/>  

<!--truncate-->

> **第二日的詳細行程可以看**[**funliday**](https://www.funliday.com/berit/journals/80835)！

## Egg Drop
韓國早餐選擇不多，  
最有名的應該是Egg Drop和Issac這兩種三明治，  
飯店樓下走路兩分鐘就有Egg Drop，三分鐘有Issac，  
第一個早上決定先吃比較多人喜歡的Egg Drop，  

<img src="https://img.poibank.com/VyYqER8IW7XrGNXVUmiYViqtudU=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZjE4ZTE1ZDktOWJjOC00YzRjLThhMWMtMDNmZWJiYzMyZDU1In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 
<img src="https://img.poibank.com/pcWlfqGqZ7M1kO6j8NvxkmKKDnE=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvY2FiZWIzZmQtN2VmZi00ZmQ0LTk5YTctOGEyOTkyNzg2YWUwIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

Egg Drop點餐就靠外面一台點餐機（只能刷卡），  
要付現金可以到櫃臺直接點餐，  
人很多所以決定外帶回飯店，  
韓國有環保政策所以外帶要多加包裝錢喔！

<img src="https://img.poibank.com/shKmsdgTadpAhNtk3ZyURu37ljs=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMGZhNWI4NmUtMmQ3Ni00ZWEwLWI0MTktNjBlZTEzNDViZGFlIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d"/>

每次出國都不會餓，  
所以我們兩個只點了一個套餐（三明治＋薯餅＋飲料）和多買一杯咖啡，  
因為BBQ口味好像蠻熱門的就選擇這個，  
結果我覺得吐司都被醬弄的爛爛的，  
不喜歡～

不過Egg Drop的飲料倒是都不錯喝！
雖然有人喝完就肚子痛～

## 我們的洞內韓式照相館
這個行程也是我們早早就在**kkday**上預訂好的，  
選的是證件照含妝髮和1對1修圖服務整套總共**2143台幣** [(點擊這裡購買↦)](https://tinyurl.com/2on8kkw3)，   
照相館在**鐘閣站**但實際地點真的不好找，  
大家一定要比對好他們提供的位置照片，  
像我們走錯入口，那間公司的櫃檯就無奈地指桌上的標語寫著「照相館在隔壁五樓。」 
想來是常常又這種狀況發生。

<img src="https://image.kkday.com/v2/image/get/w_1920%2Ch_1080%2Cc_fit%2Cq_55%2Ct_webp/s1.kkday.com/product_12804_location/20180425094436_3C4z4/png" width="65%"/> 
<img src="/img/blog/seoul-day2-1.jpeg" width="31.5%"/> 

> 圖片來源：KKday

到了正確地點之後，  
一樣先秀給櫃檯看kkday的憑證，  
當天的櫃台也是造型師，看完憑證後就帶我們去**挑衣服**，  
因為是證件照只要上半身，只需要換**上衣跟西裝外套**，  
男生可以再借領帶，女生也可以借**珍珠耳環**。 

<img src="/img/blog/seoul-day2-2.jpeg" width="50%"/> 
<img src="https://img.poibank.com/bUOwUX3I4AW9TaSr2uRr5Fo8d2w=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYjgyOWYxMmQtZWJhMy00YzZkLTkxZTYtNjAzMGE2MTZiN2Q3In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/>

換裝完就會帶去化妝室，妝髮一個人大約**40-60分鐘**，  
造型師英文不好但很努力的拿papago和我們介紹他下一步要做什麼，  
整個服務都很細心手法也很溫和，  
因為是證件照所以妝是偏自然的，不會畫太濃，  
有點可惜滿桌的眼影盤我都沒怎麼用到，  
髮型用的也是很認真，  
捷哥直接變成**韓國歐爸** :heart:  

<img src="/img/blog/seoul-day2-3.jpeg" width="50%"/> 
<img src="https://img.poibank.com/d3rGFBTK8Ct96WK-Kolv4SrMbYw=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYTUxYjYyM2QtMTFhNy00NzQxLTg5NGQtZmRmYTYxNzNjMjMzIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

造型做完之後，會先讓你填個問卷，  
問說要「跟本人像一點」還是「**好看就好**」這些，  
或是有什麼特殊修圖需求，  
例如臉要對稱、衣服不要皺摺還是要補染髮根等等。

接著就可以去攝影棚拍照，  
攝影師也服務很棒，修圖技巧超級好，  
拍的不滿意真的沒關係，攝影師會幫你**修到完美**！  
成品也很快就拿到了。

<!-- <img src="/img/blog/seoul-day2-4.jpeg" width="50%"/>  -->
<img src="https://img.poibank.com/pvfuhOGRoKcZcCiLq1KNVPdYQ7U=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMzhlZmU5YWItNTQ1MS00MjFhLTlhZTUtNjcxMTlkNjJiY2JhIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 

## 廣藏市場

拍完之後去廣藏市場， 
從**鐘路五街8號出口**出來就到了！ 
裡面觀光客和本地人都很多，  
很像在台灣**逛夜市**。

<img src="https://img.poibank.com/a9JW2EXiCrcIOlm1httumIb_oQc=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMWVhNDBkN2UtNmRjYS00N2ZmLWIyNWUtYjg2ZDY3ZmUzNGZmIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 

<img src="https://img.poibank.com/6IxPXKrwdzMk0CPB2qd-6fhgSGg=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZjdhODI3N2UtZGUwOC00ODhhLTgwYjAtZjIzOWNjYWM0N2NlIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 
<img src="https://img.poibank.com/KhP9yyNmbW4Uv1fLhZk8s0DeWVI=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMzg4OTlmMjItYmFiZC00ZjdiLTgzNTgtNjcxMGM3NDZmYmY0In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

因為這些小吃我旅伴都吃不慣，  
也沒太認真去品嚐，
指大概吃了**麻藥飯卷**、**蔬菜煎餅**和**果汁**。

<img src="https://img.poibank.com/1m_V5ENtxn-WEcu3cgB0Vsnxexc=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMGNjNmFhMmEtYzBlZC00NDM2LThkMjktZTVhZmE3YzVmNzcwIn0seyJyZXNpemUiOnsid2lkdGgiOiIxNDAwIn19XQ==" width="50%"/> 
<img src="https://img.poibank.com/sOLC-_X5JuXfqRkjJIz9BY6cHYg=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNjc4NjEwY2EtYjA3Mi00MDA5LWFkOTctM2Q3MmJjYmJkNmY1In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

來這裡的重點是買棉被！  
這間是**173號棉被店**（原169號），  
裡面也都有其他組台灣人大採購棉被，  
老闆會說一點點**中文**，
原本因為身上韓元不夠有點擔心，  
但老闆說可以用**台幣**買！只是匯率比較差(1000台幣：40500韓元)  
也可以刷卡但要付中間的處理費，  
還是用韓元現金最划算啦！

<img src="https://img.poibank.com/eBYSA18n9Rlh5bCce2aPWAne-EM=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNGNjYzkzMzItYmVhMi00Y2ViLTk2MjktYjc0NmM1NTM2M2MzIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 

我買了一件雙人冬被**65000韓元**，  
摸起來非常舒服，  
老闆會幫你抽真空弄成提袋狀（約3.5公斤），  
這樣放行李或上飛機都很方便！（真的買太多也可以請店家寄回台灣）
等拆封之後再跟大家分享～

## 南山首爾塔

把棉被丟回去飯店後就往**明洞站**出發，  
南山首爾塔這個行程就是比較尷尬，  
說特別也還好，但來首爾不去又好像不對，  
就想說還是走一趟吧～  

<img src="https://img.poibank.com/f7MBRviu0Xb8efHa8V81eWbfX50=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZjM4Njg3ZmItNjE4YS00NWRlLWIyM2MtODY3YTFiMDIyNDJjIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="60%"/> 

我們上塔的方式是坐**電梯**到**纜車**搭乘的地方，  
再搭乘纜車到塔底，然後就不上塔ＸＤ
有體力的人可以考慮用走的上山邊看沿路風景，  
或是搭公車上去但要研究一下。

搭車到明洞要走一大段路才會到纜車電梯處，  
電梯一次容納大概10個人，一趟來回大概5分鐘，  
還好當時人不算多，大約排20分鐘到纜車購票處買票，  
現在纜車撘一次來回要**14000韓元**，  
漲價漲了不少啊！

<img src="https://img.poibank.com/LmWicMBvB-VcOfRj82dQCPSx8pw=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYzdiODU2ZWQtYmMwOS00Mjc3LWFhNTgtMTM3MTMyZjNiNDdkIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 
<img src="https://img.poibank.com/Iwkez8XZOOFAy0WcKylUeJuQOQY=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvM2E2NWI3NGItYTZiMi00YjEyLTk5NWMtYTExM2E0NmE2NTdjIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 


纜車大約排15分鐘就搭到了，  
不會很晃，可以看看下面的樹和櫻花也不錯～

<img src="https://img.poibank.com/Ky0V4xyOZFkQDYIs7n1Tgc7b5V8=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNzM3Njc1NDAtNWFhMi00YjcxLThlNGMtMWU5ODI0NzEwM2I5In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 
<img src="https://img.poibank.com/mC9IU0nWOAOZOpLI1G1hCF5-Ti4=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZjM0YjIxYzktOGI5Zi00MzdhLWFmZDItZDk1ODEyYWQwNDQyIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 


上去之後，因為高度也蠻高的了，  
就不花錢上塔了，省點錢等一下去吃燒肉！  
塔上也蠻無聊的，  
就看看**首爾市區風景**，看看亂掛的**愛情鎖**，  
逛逛**紀念品店**就差不多了。

<img src="https://img.poibank.com/-iAQZ3bVzU-nhFSNFX3qKsprSw4=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNmQ3OWFkZTYtNjQxNS00YTkzLTlmNDgtNmMxMjNmNzE0ZjFlIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="100%"/> 
<img src="https://img.poibank.com/pNpAoYmRIPNzdRgQlmmTq8WjA_Q=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvYmU4MmIyODUtNDU5Zi00NjliLWE2NDItY2FiMTM3YzFlZTQzIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 
<img src="https://img.poibank.com/Tiu9ZQfBrSYy81qtyGquNPG2i5M=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMDIwYjRiYjUtYmViMy00MTQ0LWIxN2YtZDdiZjA4ZmU5MDQ2In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

下山後就開始在明洞到處逛逛，  
開始購物啦～

## 王妃家
晚餐選擇到王妃家吃韓式烤肉！  
王妃家在明洞雖然有三間分店但是座位都不多，  
我們比較早到（不到六點）因此不用排隊！  

畢竟是旅遊熱門景點，在場都用中文服務生服務，  
菜單也都有殘體中文可以看，  
王妃家算是比較高級的烤肉，單價會比較高一點，  
我們點了一份必吃的韓牛和一份必吃的豬五花和一份解膩用香菇拼盤！

<img src="https://img.poibank.com/VyIP5BxMEsKeIDXeXpeJZhyu2A8=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNTQzM2YxYmItZjE3Mi00MjQzLThkYjktZjQ0ZTRhZWY5MjA4In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d"/>

點完餐就會開始狂上小菜，  
王妃家小菜超級無敵多，  
因此兩人這樣吃還算可以飽。  

<img src="https://img.poibank.com/1-KWfkptd1zW3J6iXJG7Cf2UVGE=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvMzIzYTgzMjgtNTc3Ny00Mzk5LWIwNDUtOTIxMjU1ZTRkZjkwIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 
<img src="https://img.poibank.com/S7HdU9DdLEjCgfWxF3KpXivtkcY=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvZWFiYzdlNjAtZDc0Ni00NzRhLWFmMTUtYzUyYzUxZjA1YzE1In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

滿滿精緻的小菜啊！  
角落一坨像冰淇淋那盤是南瓜泥，口感滑順超好吃！  

<img src="https://img.poibank.com/2avt8Vf62N_P73bM5i9IPKlis7Y=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvN2ZjOWM5OTEtNTRhYy00M2M0LWEwYzEtY2VkOTlkYzllOGE3In0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 
<img src="https://img.poibank.com/rVb_2R7k1psPZNE4gpADfFqf3wY=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNTlmY2Q1NzQtMTU1Yy00NjdhLWE3MTEtMjQwN2M1OGFkM2VhIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d" width="50%"/> 

捷哥點了燒酒，但一個人喝太孤單所以沒喝完XD  
比較紅的是牛肉，粉粉的是豬肉，含有可愛的菇！  

<img src="https://img.poibank.com/LoNY8rm4pOmmiIaOVbXxd7hxNx8=/W3siZm9ybWF0Ijoid2VicCJ9LHsia2V5Ijoiam91cm5hbHMvODA4MzUvNjhiYjI5NmEtOTg0MC00ZmI2LWJiZTQtZGYzOTZhNmMyYTJhIn0seyJyZXNpemUiOnsid2lkdGgiOiI4MDAifX1d"/>

店員烤肉技術很好，  
尤其必須大推烤蘑菇，  
他們會把蘑菇烤到外酥內嫩，  
而且會保留蘑菇汁！  
那個是我覺得韓國烤肉裡最特別的一道菜！



## ☺ 第二天總結
腳已經快要不行了 :feet:

:::note 韓國旅遊相關

[北村韓屋村](https://peiyu.us/blog/2023-korea-last-day)   
[韓服體驗＋景福宮](https://peiyu.us/blog/2023-korea-third-day)  
[韓國校服體驗＋石村湖賞櫻](https://peiyu.us/blog/2023-korea-first-day)  
[韓國行前住宿交通換錢等須知](https://peiyu.us/blog/2023-korea-preparation)  
[韓國入境相關資料](https://peiyu.us/blog/2023-korea-keta-qcode)  
[韓國WOWPASS申請教學](https://peiyu.us/blog/2023-korea-wowpass)   

:::