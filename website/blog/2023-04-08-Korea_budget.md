---
slug: 2023-korea-budget
title: 2023 || 韓國旅遊 | 首爾四天三夜費用公開
tags: [美食,體驗,旅遊, 韓國旅遊,2023]
comments: true 
---

【 2023.04.08 韓國旅遊四天三夜費用 】

去韓國一趟的費用加省錢小技巧！

<img src="/img/blog/korea_budget.png" width="100%"/> 

<!--truncate-->

## 省錢小技巧
開頭先來說如何**省小錢**！  

### 預定行程
在[**KKday**](https://tinyurl.com/2fz5xwxs)或是[**Klock**](https://tinyurl.com/2nypml8u)上先**預定好行程**，  
通常會比在當地現場買便宜喔！  
像是搭乘機場快綫[AREX](https://tinyurl.com/27f96s5c)一次可以**省30元**，  
[韓服體驗](https://tinyurl.com/2lwad5hx)可以**免費多穿**兩個小時等等的優惠，  
因此推薦大家能買的就先買！

### 信用卡回饋
我主要用兩張信用卡來消費，  
分別是**台新黑狗卡**和**國泰cube卡**，  
在韓國都有0.3%現金回饋無上限，  
加上這兩張卡刷[**KKday**](https://tinyurl.com/2fz5xwxs),[**Klock**](https://tinyurl.com/2nypml8u)或是[**Booking.com**](https://tinyurl.com/2q2744j4)都可以領到更多回饋，  
越刷省越多啊～

### WOWPASS回饋
如果透過優惠碼申辦**wowpass**，  
就可以在儲值外幣時想**0.5%的現金回饋**，  
而且在特定店家使用wowpass付款也可以另外再享現金回饋！  
請參考：[韓國WOWPASS申請教學](https://peiyu.us/blog/2023-korea-wowpass)  

### 蝦貝回饋
如果行程是[**SHOPBACK蝦貝**](https://app.shopback.com/d8I9F4qX2Ab)這個APP進入來購買，  
可以另外賺回饋，  
只需要先進來SHOPBACK再點擊[**KKday**](https://tinyurl.com/2fz5xwxs)或是[**Klock**](https://tinyurl.com/2nypml8u)進入，  
接著就像一般在購買票卷那樣購買就可以領到回饋了！


因此，終極小技巧就是：  
**先從SHOPBACK進入booking/kkday等網站再用回饋高的信用卡付款！**  
例如我的飯店費用  
原價5004元 - 蝦貝回饋276元 - 信用卡回饋150元 = 最終只要4578元  
又少了**400多元**！

## 住行
去除掉購物這次去韓國一人的話費大約是**2萬5**左右，  
最主要的話費也是在機票**1萬5**。  
如果可以搶到聯航便宜機票的人就可以大省一筆！


再來是住宿三個晚上雙人房5004元，  
因為是一般旅店因此非常便宜，東西還應有盡有，地點又方便，  
主要是我認為在韓國用booking訂飯店比用Airbnb有保障一點，  
平均下來一個人一晚不到一千我覺得CP值非常高！
非常推薦來這間東大門的[ **Hotel DDK** ](https://tinyurl.com/2h98o29t)!

首爾的交通費也不貴，  
機場到市區的那段稍微比貴一點點，  
搭Arex一趟182元 （線上購買更便宜 [點擊這裡購買↦](https://tinyurl.com/27f96s5c)），  
地鐵四天來來回回到處亂搭總共花**不到450元**台幣，  
建議事先稍微算好要搭幾趟才不會加值太多用不完或一直找機器加值！  

## 食樂
食物總花費一人約2500元，  
除了王妃家單價稍微高一點之外 (一人約800元)，  
其他餐**300-500元**都可以解決，  
當然這是至少兩個人的情況！  
在韓國一個人吃飯真的不太方便。

玩樂的費用稍微高一點點，  
因為整套裝髮加證件照就要2160元，  
其餘活動大約500元就可以玩得很開心。

> 感性校服：443元 （+116 另外租外套  
> 證件照：2160元  
> 南山纜車：328元  
> 西花韓服：554元  

## 其他
其他就例如說網卡、置物櫃、旅平險和行李寄送服務，  
就是比較看個人要不要花這些小錢摟！

## 總結花費
不加購物購物的花費為26610元，
加上購物的話我一個人花了41490元，  
**但現金回饋也足足領了1190元**。



:::note 韓國旅遊相關

[韓服體驗＋景福宮](https://peiyu.us/blog/2023-korea-third-day)  
[韓式證件照＋南山首爾塔](https://peiyu.us/blog/2023-korea-second-day)  
[韓國校服體驗＋石村湖賞櫻](https://peiyu.us/blog/2023-korea-first-day)  
[韓國行前住宿交通換錢等須知](https://peiyu.us/blog/2023-korea-preparation)  
[韓國入境相關資料](https://peiyu.us/blog/2023-korea-keta-qcode)  
[韓國WOWPASS申請教學](https://peiyu.us/blog/2023-korea-wowpass)  

:::