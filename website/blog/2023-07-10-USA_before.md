---
slug: 2023-usa-before
title: 2023 || 美國旅遊 | 行前須知 + ESTA申請
tags: [簽證, 旅遊, 美國旅遊,2023]
comments: true 
---

【 2023.07.10 美國行前準備 ESTA申請 文件準備 網卡選購 】

今年有幸被邀請到美國真菌學會演講，所以就順便安排了一趟沙漠公路之旅。  
上一次去美國已經是12年前了，現在規定已經完全不一樣了！  
加上這次入境時會是一個未婚女子的狀態，擔心移民官覺得我會跳機？  
所以行前準備要做的充足一點才可以啊～


<img src="/img/blog/usa-esta.png" width="100%"/> 

<!--truncate-->


:::note 2023美國入境須知

* 美國 ESTA 免簽申請
* 免隔離、免PCR、免疫苗證明、不需強制戴口罩
* 入境相關文件備用

:::

# 美國出差旅行行前須知
### 行李必備
飛美國通常可以帶兩大咖行李箱，  
但由於我主要是去出差加上夏季衣物都很輕薄，  
所以一咖都沒裝滿...  
在桃園機場登機時量才**8公斤**而已。  

去美國也**不需要準備轉接頭**，  
而且基本上有**Apple pay或是信用卡**就暢行無阻，  
連美金也不用換半毛錢～
 

> 證件：護照、身分證（護照遺失需要身份證）  
> 電子：手機、網路或wifi  
> 行程：機票、訂房、邀請函  
> 貨幣：信用卡 


### 機票
研討會的時間在暑假期間，    
機票都 超 級 貴 ！  
最後選擇直接坐**華航**直飛洛杉磯，  
票價來回一個人約七萬。

:airplane:  7月27日 晚上11:50去  
:airplane:  8月4日 晚上12:45回  


### 入境文件
因為怕移民官問比較多問題，  
建議把能準備的文件全部準備好，  
即便像我一樣最後準備的東西一樣也沒被檢查，  
還是要以便不時之需！  

我在LAX機場從下飛機開始到領到行李花了差不多1個小時，
所以大家要有心理準備入境之路真的很漫長。

#### ESTA申請
旅行授權電子系統 (Electronic System for Travel Authorization) 簡稱**ESTA** ，  
台灣是美國免簽證計劃的授權國，所以**無需申請美國簽證**，  
但需要申請ESTA才能以入境美國。

要前往美國出差或觀光（停留時間不超過**90**天）或是過境美國都要申請！
要在飛機起飛前72小時**之前**申請！  

❗ESTA的效期為**2年**或是護照到期日，過期了要再次申請喔❗

#####  = 教學 =
申請總共有7個步驟，填寫時間越15-30分鐘，費用為21美金。  
大約**一天至半天**的時間就可以收到申請結果。

ESTA需要在[**官網**](https://esta.cbp.dhs.gov/)申請也可以用**app**申請，  
如果用電腦要注意到底是不是官網，還是詐騙的代辦網站！

進入官網之後可以選擇 🇹🇼 **中文** 來回答問題比較方便，
<img src="/img/blog/usa-esta/1.png" width="100%"/>

接著點選創建新的**個人申請**就可以開始啦！
<img src="/img/blog/usa-esta/2.png" width="100%"/>

➊ 第一頁為免責聲明，閱讀完後按**同意**就可以進入下一頁。  
<img src="/img/blog/usa-esta/3.png" width="100%"/>

➋ 第二頁要填寫**護照資訊**，可以直接拍照或上傳來掃描，節省輸入時間跟眼殘錯誤。
<img src="/img/blog/usa-esta/4.png" width="100%"/>
如果有沒有辨識掃描到的，還是可以手動輸入資訊，  
也記得順便檢查一下自動輸入的資訊有沒有錯誤！
<img src="/img/blog/usa-esta/5.png" width="100%"/>

最後要進行**電子郵件的驗證**，輸入完後會在信箱收到一封有驗證碼的信，  
再把驗證碼輸入到回申請頁面就可以完成驗證。
<img src="/img/blog/usa-esta/6.png" width="100%"/>
<img src="/img/blog/usa-esta/7.png" width="100%"/>
<img src="/img/blog/usa-esta/8.png" width="100%"/>


➌ 電子郵件確認成功後，第三頁要填寫**個人資料**，  
英文地址可以到[**郵局官網**](https://www.post.gov.tw/post/internet/Postal/index.jsp?ID=207)查詢，  
除了常見的聯絡資訊還要填寫**社交媒體**的帳號，也不知道是不是真的會審查 ＸＤ
<img src="/img/blog/usa-esta/9.png" width="100%"/>
<img src="/img/blog/usa-esta/10.png" width="100%"/>

➍ 第四頁要填寫入境後的資訊，包含在美國的聯絡人和住址，  
因為我是去參加會議，因此聯絡人我填寫了會議負責人，  
住址就填寫**飯店**的資訊就可以！
<img src="/img/blog/usa-esta/11.png" width="100%"/>
<img src="/img/blog/usa-esta/12.png" width="100%"/>

➎ 第五頁為勾選一些資格問題，只要**照實填寫**就可以。
<img src="/img/blog/usa-esta/13.png" width="100%"/>
<img src="/img/blog/usa-esta/14.png" width="100%"/>

➏ 第六頁為資料確認，只要確定你的資料都沒有打錯就可以進入最後一步驟！
<img src="/img/blog/usa-esta/15.png" width="100%"/>

➐ 最後當然就是**付款**啦～  
目前頁面應該是顯示尚未付款，點選繳納按鈕後就可以進行信用卡的付款，  
2023年的價格為**21美金**。
<img src="/img/blog/usa-esta/16.png" width="100%"/>
<img src="/img/blog/usa-esta/17.png" width="100%"/>

付完款後就會顯示申請正在處理中也同時會收到一封通知信，  
記住**申請號碼**的話可以隨時進入網站查詢申請狀態！  
<img src="/img/blog/usa-esta/18.png" width="100%"/>

要查詢狀態的話只要回到首頁，  
這次點選查詢，
<img src="/img/blog/usa-esta/19.png" width="100%"/>
輸入護照資訊加上申請號碼，就可以看到ESTA申請狀態。  
<img src="/img/blog/usa-esta/20.png" width="100%"/>

申請結果會再寄一封通知信到驗證的信箱，  
就可以知道最後你的ESTA是否有被**核准**！
<img src="/img/blog/usa-esta/21.png" width="100%"/>

我有把ESTA影印下來以備不時之需，但是最後沒有用到。  


#### 其他文件準備
其都文件我也都有備著，畢竟希望可以順利入境，  
準備多一點都是好的～

因為我是去出差參加會議，  
因此需要準備：
* 邀請函
* 會議議程
  
其他文件就是一般旅遊也會準備的：
* **回程機票（最重要！）**
* 訂房憑證

這些都準備好基本上不怕遇到什麼問題了！


#### 住宿
除了會議幾天的住宿，  
訂房我都是直接在[**Booking.com**](https://tinyurl.com/2q2744j4)上找預算內的旅館，  
找那種連鎖的hotel通常比較保險，雖然位置可以偏僻一點，   
但美國住宿實在是有夠貴...

這次大部分都住在Hilton的連鎖旅館，  
還有一次因為臨時換行程訂的La Quinta也是平價連鎖旅館，  
住的感受都不錯！就是偏貴而已～

<details>
  <summary><b>這趟旅行訂的飯店跟當時的每晚價格</b></summary>
  <div>
    洛杉磯 (LA,CA)：<a href="https://guesthouse.ucla.edu/">UCLA Guest House - $249  </a><br></br>
    棕櫚泉 (Palm Springs,LA)：<a href="https://tinyurl.com/ykcmqopq">Hilton Garden Inn Palms Springs - $102 </a><br></br>
    鳳凰城 (Phoenix,AZ)：<a href="https://tinyurl.com/ytdgg8lk">Home2 Suites By Hilton - $132 </a><br></br>
    金曼 (Kingman,AZ)：<a href="https://tinyurl.com/yt3pjfck">La Quinta by Wyndham Kingman - $157 </a><br></br>
  </div>
</details>


#### 行程
行程規劃我一樣是用[**funliday**](https://www.funliday.com/berit/trips/64194ab618e0bc004cb53fd5)來做規劃，  
畢竟這次是公路旅行，funliday有地圖和時間預估功能，  
雖然最後行程因為天氣因素大改一波，  
但在規劃上還是很推薦使用！

<iframe title="2023｜美國｜大峽谷沙漠公路之旅" src="https://www.funliday.com/berit/trips/647fd57eea9aee00314fca1f/embed?hl=zh_tw" width="100%" height="360" frameborder="0"></iframe><p></p>

大峽谷的行程是會議安排的，  
是和[**Grand Canyon Adventures**](https://www.dothecanyon.com)購買的行程，  
行程為**Grand Canyon Guided Tour**需要$174美金包含午餐跟點心飲料，  
總時長為8小時，基本都坐在車上跑點，非常輕鬆適合給不想曬太陽跟走路的人！

### 小費文化
美國的小費文化我實在是不太認同，  
但也只能入境隨俗～  

基本上就是外帶可以不用給小費，  
內用基本15%小費，服務的好的話20%，  
參加行程有導遊的話也要給小費，  
房間有過夜的話也要給小費，  
但如果跟我一樣住一晚的我就沒有給了～  

### 美國交通
#### 租車
在美國，沒車真的超不方便，  
加上這次需要跨州移動，**租車**還是最方便的。

租車是在機場旁邊的**Avis**直接租車，  
但因為不是我負責的部分，沒辦法提供什麼心得，  
只能說 一定要確定冷氣夠冷！  
他會讓你在公路旅行中減少非常多痛苦！

### 美國上網
因為這次會經過荒蕪的沙漠跟冷門的地點，  
我特別去找那種支援多家電信公司的eSIM，希望網路隨時保持順暢，  
我建議最少也要支援兩個以上的電信公司，尤其最好是美國最大的兩家電信AT&T和T-Mobile，  
最後我選擇使用[KKday上購買的**eSIM**](9https://tinyurl.com/yoq2q9aq) 10天500MB吃到飽466元方案！  
([點擊這裡購買↦ 美加網卡｜美國、加拿大 每日高速型500MB無限流量，總量型10日 eSIM](https://tinyurl.com/yoq2q9aq))  

最後在整趟沙漠公路之旅中很少有完全抓不到網路的狀況，  
有幾次發生也真的是因為那裡太過荒涼...
整體使用感覺很好，非常推薦！
<img src="/img/blog/usa-esta/22.png" width="100%"/>


大致上這次出差準備的東西就是這樣，  



## 祝大家旅快！:black_heart: