---
slug: Travel-to-Glasgow
title: 2019 || 英國旅遊 | 格拉斯哥生態研討會
tags: [Field trip, UniofAberdeen, Glasgow, Scotland,UK, 英國旅遊,2019]
---

【2019.04.02】蘇格蘭生態研討會
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56386506_2651182314899072_7344096628199915520_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=0debeb&_nc_ohc=aub95D79cGUAX9W8pvz&_nc_ht=scontent-tpe1-1.xx&oh=00_AT9X99KnFSjYM-t4udYRR8kScpLOi68ua9VZUcrRI2AO7g&oe=6347EF9B)

<!--truncate-->
# 研討會
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56664715_2651170961566874_8165297738154508288_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=0debeb&_nc_ohc=h1hrxXxfsPUAX8BNWn-&_nc_ht=scontent-tpe1-1.xx&oh=00_AT8qYW9DCXH64Ud2x_rvaIf7YTDWvqLdNRWz4Y5RBl4hPA&oe=6348FD88)
去格拉斯哥大學參加蘇格蘭一年一度的生態研討會
外國人大多可以很自然從容的侃侃而談
但常常變成沒有任何語調15分鐘很難專注聽下去
還有因為鳥族群減少的餵食計畫也讓我不太理解（這個得獎了呢）
但還是有一些優質的報告和有趣的題目
不過大部分是有關魚和水裡動物的

在海報報告的時候看到一篇海報突然整個熟悉
發現這個英國人的樣區竟然在台灣
她還跟我說台灣的食物很好吃
我說我知道我好想念台灣的美食

再來可能是配合生態環境主題
研討會提供的餐飲都是素食的 我同學說非常好吃
我覺得他們味覺壞掉了 我整個吃不下去
連提供的咖啡也難喝到想哭

![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56605008_2651171038233533_9062362912747683840_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=0debeb&_nc_ohc=ejexwn0lK8cAX_wsQaX&_nc_ht=scontent-tpe1-1.xx&oh=00_AT99BVGu4m6Aqru_Uc26R9dehHa3p1pOB1veW4ffjRgEaw&oe=63482645)
我們班的人都住在一間一樓有酒吧的青年旅館
環境不錯 價格又便宜 很推薦！

住宿：Euro Hostel Glasgow
價格：£36/人
推薦：：★★★★☆ 

-------
研討會結束之後把男朋友召喚來才是目的
可以到處觀光遊玩了

# 格拉斯哥
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56427027_2651182158232421_9090397444188930048_n.jpg?_nc_cat=105&ccb=1-7&_nc_sid=0debeb&_nc_ohc=TSMWo4q5AzMAX-F8-W8&_nc_oc=AQmFql8zvirOyGn1jzgWLAvnIevA4f3Q1ycFABppythEEl6dg0rjt4wb0eNZMtvBSgQ&_nc_ht=scontent-tpe1-1.xx&oh=00_AT_Lamd1Tju7Tqfcwfs_NrOQFnHRBXPEBW-kCbdsZgISAA&oe=634485D5)
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56461039_2651182191565751_9008847079073644544_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=0debeb&_nc_ohc=XyfDVZbmg-sAX_Xmkw5&_nc_ht=scontent-tpe1-1.xx&oh=00_AT-mHKdhZISwx5YZf43LVpcpiJlVDtVrO0FUqqXJQd5qiA&oe=634908F2)
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56400456_2651182244899079_4328678640335192064_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=0debeb&_nc_ohc=iQlZCuFJ1nwAX_jqvUz&_nc_ht=scontent-tpe1-1.xx&oh=00_AT8vVeITAoxrs-Yw1eOZH8AvHCAOQuzt0k2zBOOrzECpRw&oe=634815D1)
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56509938_2651182151565755_1134233616570646528_n.jpg?_nc_cat=109&ccb=1-7&_nc_sid=0debeb&_nc_ohc=WHFg_TNDje8AX-2qgIy&_nc_ht=scontent-tpe1-1.xx&oh=00_AT-FXWzvUH2ftfJ4w9iPtwug9B8w-26qww4LecqG7E2Xkg&oe=6346F16D)
## 格拉斯哥大教堂
接到從機場來的男朋友後我們就到了漂亮的教堂 
這裡拍攝過異鄉人 outlander 
裡面就是漂漂亮亮大教堂 
有美麗的彩繪玻璃窗
還有人在彈奏管風琴
捐錢也很先進 可以電子支付

景點：[GLASGOW CATHEDRAL](https://www.glasgowcathedral.org/visiting/)
價格：free
推薦：：★★★★☆ 

![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/57070977_2651182201565750_1054406907730591744_n.jpg?_nc_cat=103&ccb=1-7&_nc_sid=0debeb&_nc_ohc=OdRd5eYEqIoAX-naOEb&tn=pObl2N0ma86qw50h&_nc_ht=scontent-tpe1-1.xx&oh=00_AT_dodurEDtjA9tI_KJOJhVkJdyBYWMTa6bzODh6knf3Yg&oe=63458406)
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/56819125_2651182238232413_6812902657942880256_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=0debeb&_nc_ohc=gkdjDIkoumsAX_TnfNQ&_nc_ht=scontent-tpe1-1.xx&oh=00_AT8TfMpqV-QWudg61k8v8SUtZsQ6uwc4L2JcPs84mt-_Ag&oe=6346E85A)
## 聖穆格宗教博物館
歐洲窮遊攻略 逛完教堂、換逛博物館 都不用錢錢喔！
這是因為景點剛好在旁邊然後我腳酸了
需要找個地方休息所以去的
除了可以休息以外對我來說沒說麼特別的

景點：St. Mungo Museum Of Religious Life & Art
價格：free
推薦：：★★☆☆☆ 

因為時間不多
我們也早早回Airbnb休息
這次訂的房間超大 一房一廳一廚房一衛浴
原本覺得很棒
結果狂跳電 最後連熱水都沒有
