---
slug: Last-Field-trip 
title: 2019 || 留學日記 | 最後一次野外課程
tags: [Field trip, UniofAberdeen, Glen Tanar, Scotland,UK, 留學日記,2019]
---

【2019.03.16】亞伯丁大學-生態與保育研究所-野外課程
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/54206277_2613551928662111_4198692850986123264_n.jpg?_nc_cat=108&ccb=1-7&_nc_sid=730e14&_nc_ohc=EP19AWSA-XUAX_NvXXC&_nc_ht=scontent-tpe1-1.xx&oh=00_AT_u-BLCO4SpI546DBQgbD40MdaYJEvPw1o9Mwrn3tOAXw&oe=63469267)
<!--truncate-->

這一年來校外教學好幾次，但最常來的就是這個塔納河谷 [Glen Tanar](https://www.glentanar.co.uk/)
雖然說來這麼多次，植物因為季節的變化差別還是很大
每次去都會學到很多新東西

![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/54523489_2613551988662105_7195312192198017024_n.jpg?_nc_cat=105&ccb=1-7&_nc_sid=730e14&_nc_ohc=Ve9x_j6eoPgAX_RdAVq&_nc_ht=scontent-tpe1-1.xx&oh=00_AT-QHqUwGWH-PyJzqvQevbSz5WUoEW6mZDaeDgQd8Pe1Eg&oe=63467D56)
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/53934069_2613552058662098_6248872734994989056_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=730e14&_nc_ohc=Rq_mjmxKmmwAX9AP98Z&_nc_ht=scontent-tpe1-1.xx&oh=00_AT95HahhBBgv0YPpLYbwjnVDq48GoZ9Nz5xZVlzJNsycxA&oe=63470C51)
![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/54217308_2613552101995427_3226441409133281280_n.jpg?_nc_cat=111&ccb=1-7&_nc_sid=730e14&_nc_ohc=lgJhVH-GP7AAX9TAEI0&_nc_ht=scontent-tpe1-1.xx&oh=00_AT_X1PduRnB6lzFYf7fN-L3UMgvfhPV3KLxcuyS-Bjj2QA&oe=63445691)

植物生態學課就會來這裡學習植物認識和調查
森林學課就要來這裡學怎麼當種樹農夫致富
環境管理課也要來這裡看環境影響

來挖土、找菇菇、找苔蘚需要學任何東西都來這裡實地演練一番

印象最深刻的是春天的時候要去認識葉芽
這個就真的不容易，因為他們都長得一樣啊

上山丘下沼澤
很感謝我的隊員凱瑞

![](https://scontent-tpe1-1.xx.fbcdn.net/v/t1.6435-9/53916338_2613551858662118_4541031847903100928_n.jpg?_nc_cat=105&ccb=1-7&_nc_sid=730e14&_nc_ohc=gDN3tdyqgWkAX_RoO7Z&_nc_ht=scontent-tpe1-1.xx&oh=00_AT8N8F6I2DCFuhHjz3sruzOr8mbTdOHt14rVhYQie-7_bg&oe=634579B7)