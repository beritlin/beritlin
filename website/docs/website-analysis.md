---
title: Use Google Analytics with your site
---

Docusaurus support [Google Analytics](https://analytics.google.com/analytics/web/) with tow plugin `plugin-google-analytics` and `plugin-google-gtag` **(recommend!)**.It can measure how users interact with your website

:::note
`plugin-google-analytics` is deprecated, and will become useless on July 1, 2023.
`UA-*` tracking id will also be removed and change to `G-*` id.
:::

## step1: install plugin

```bash
yarn add @docusaurus/plugin-google-analytics @docusaurus/plugin-google-gtag
```
> make sure the version is the same as your Docusaurus
> 

and add the following code to your `docusaurus.config.js` file.
```jsx title="docusaurus.config.js"
module.exports = {
  presets: [
    [
      '@docusaurus/preset-classic',
      {
          // highlight-start
        googleAnalytics: {
          trackingID: '**********',
          anonymizeIP: true,
        },
        gtag: {
          trackingID: 'G-**********',
          anonymizeIP: true,
        },
         // highlight-end
      },
    ],
  ],
};
```

## step2: create a new Google Analytics
Go to [Google Analytics](https://analytics.google.com/analytics/web/) and select `Go to stream setup`. Then create a new property for collecting data.

<img src="/img/doc/Analytics2.png" width="50%"/><img src="/img/doc/Analytics3.png" width="50%"/>

Entre your sites URL and the name, google will automatically generate the id.
<p align="center"><img src="/img/doc/Analytics4.png" width="100%"/></p>

## step3: find your tracking id
The measurement id is for `gtag` plugin and the property id is for `analytics`.
<img src="/img/doc/Analytics5.png" width="50%"/><img src="/img/doc/Analytics6.png" width="50%"/>

Finally, you can view your site analysis results on the page!
<p align="center"><img src="/img/doc/Analytics1.png" width="100%"/></p>