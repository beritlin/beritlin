---
title: Phylogenetic Diversity
---
Traditional species diversity does not the similarity between species. 

## Faith PD
Sum of the branch lengths of the phylogeny connecting all species from tips to root. It does not incorporate species abundance.

## Functional diversity