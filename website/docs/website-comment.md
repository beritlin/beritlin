---
title: Add Cusdis to Your Website
---

Docusaurus does not provide a message/comments function. To add such function, it would require other third-party message plugins. There are some plugins link to the social media like Disqus (Facebook) or Giscus (GitHub) but it may result in some privacy issues. **Cusdis** is your alternative option which is an open-source, lightweight (~5kb gzipped) and safe comment system for your website.

## step1: sign up for Cusdis
Go to [Cusdis website](https://cusdis.com) and add the new website to the dashboard. Then click the `Embed Code` button in the dashboard for checking the app ID. The ID will be used in the final setting.
<p align="center"><img src="/img/doc/cusdis1.png" width="100%"/></p>

## step2: install plugin
Cusdis provides the react version so that it can directly be used in Docusaurus.

```bash
yarn add react-cusdis
```

## step3: make a copy of your script by Swizzle
Swizzle DocItem and BlogPoseItem.

```bash
yarn run swizzle @docusaurus/theme-classic BlogPoseItem --danger
yarn run swizzle @docusaurus/theme-classic DocItem --danger
```

## step4: modified the script
Navigate to `src/theme/` and edit the `index.js` file from `BlogPoseItem` and `DocItem` folders as below.

```js title=".../website/src/theme/BlogPoseItem/index.js"
import React from 'react';
import BlogPostItem from '@theme-original/BlogPostItem';
import { ReactCusdis } from 'react-cusdis'

export default function BlogPostItemWrapper(props) {
  return (
    <>
      <BlogPostItem {...props} />

         <ReactCusdis
          lang="zh-tw" //繁體中文
          attrs={{
          host: 'https://cusdis.com',
          appId: "{{ app_id }}", // change to tour app ID, see step 1
          pageId: "{{ PAGE_ID }}",
          pageTitle: props.title,
          pageUrl: 'https://peiyu.us/' + props.siteUrl, // change to your website
          }}
          />

    </>
  );
}


```

```js title=".../website/src/theme/DocItem/index.js"
import React from 'react';
import DocItem from '@theme-original/DocItem';
import { ReactCusdis } from 'react-cusdis'

export default function DocItemWrapper(props) {
  
  return (
    <>
      <DocItem {...props} />

         <ReactCusdis
          lang="en-UK" // English
          attrs={{
          host: 'https://cusdis.com',
          appId: "{{ app_id }}", // change to tour app ID, see step 1
          pageId: "{{ PAGE_ID }}",
          pageTitle: props.title,
          pageUrl: 'https://peiyu.us/' + props.siteUrl, // change to your website
          }}
          />

    </>
  );
}
```
Then the comment frame will be shown on your website!
<p align="center"><img src="/img/doc/cusdis2.png" width="100%"/></p>
