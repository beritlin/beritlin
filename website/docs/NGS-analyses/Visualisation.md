---
title: Genomic Data Visualisation
---

The genomic data can be visualised by the **Integrative Genomics Viewer** [(IGV)](https://software.broadinstitute.org/software/igv/). It supports flexible integration of all the common types of genomic data and metadata, investigator-generated or publicly available, loaded from local or cloud sources.

