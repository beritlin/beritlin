---
title: Gene Expression Analysis
---

The collectively of RNA, specifically the messenger RNA (mRNA), is called transcriptome. These amount of mRNA transcribed from am individual gene at any particular time or tissue type is able to measure the gene expression.

<p align="center">
<img src="/img/NGS/rnaseq.png" width="500"/>
</p>
<p align="center">
Fig1. Schematic of RNA-seq library construction
</p>

## Pipeline 
### Step1: Alignment
The popular RNA-seq aligner hisat2 <sup>[2](http://daehwankimlab.github.io/hisat2/)</sup> is used for mapping. First, the hisat2 index need to be built beforehand by using  `build`.
```
# build index
hisat2-build genome.fa genome
# mapping 
hisat2 --dta-cufflinks -x genome -U RNA_test.fastq.gz -S RNA_test.sam
```
The output of building index are a list of ht2 index files such as genome.1.ht2, genome.2.ht2, genome.3.ht2 …


### Step2: Identify DEGs
DEGs can be found simply using the tool cuffdiff [3](http://cole-trapnell-lab.github.io/cufflinks/cuffdiff/) by input the samples (and replicates) for comparison. 
```
cuffdiff -o cuffdiff -L WT,MT genes.gtf RNAc_WT_a.bam,RNAc_WT_b.bam
RNAc_MT_a.bam,RNAc_MT_b.bam
```
Two text files will be generated including a DEGs list gene_exp.diff and a Fragments Per Kilobase per Million (FPKM) for each sample genes.read_group_tracking.

### Step3: Visualisation



Raw counts
RPKM (Reads Per Kilobase per Million)
FPKM (Fragments Per Kilobase per Million)
TPM (Transcripts Per Million)