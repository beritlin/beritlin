---
title: Submit Data to GEO
---

**G**ene **E**xpression **O**mnibus (GEO) is a public genomics data repository that facilitates data submissions adhering to the MIAME standards. It accepts both array-based and sequence-based data and provides tools to assist users in querying and downloading gene expression profiles for experiments and studies.


Essentially, the original sequencing data from articles is often required to be submitted to this database.


## Uploading your dataset
### Step1: Preparing your submission
➊ Visit the [ NCBI homepage](https://www.ncbi.nlm.nih.gov/) and navigate to **Submit** page.
<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_1.png?raw=true" width="100%"/>
</p>

➋ Scroll down to **Other Toools** and locate the **GEO** box.

<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_2.png?raw=true" width="100%"/>
</p>

➌ Select the data type that you want to upload (e.g., NGS data) and click the **Submit high-throughput sequencing**.

<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_3.png?raw=true" width="100%"/>
</p>

➍ Then, select **Raw data files**.

➎ Run an md5 check for data integrity during transfer, the md5 should also be provided to GEO.

```bash
md5sum *.fastq.gz > md5.txt
```

### Step2: Transfer your data
There are two steps for GEO submission.   
➊ GEO submission involves two steps. Firstly, transfer all your files to the GEO server. Navigate to **Transfer Files**.
<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_4.png?raw=true" width="100%"/>
</p>

➋ Login to NCBI and GEO would then assign the path for data uploading in their **step 1.** protocal, as well as the FTP username & password in **step 2.**.
<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_8.png?raw=true" width="100%"/>
</p>
<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_9.png?raw=true" width="100%"/>
</p>

➌ Multiple options are available for dataset upload. Here, I recommend using SFTP for transferring data from local to another server.

```bash
### In your local terminal, login to GEO FTP server
sftp geoftp@host_address # host_address is in step2.
# entre the password GEO provided

### Create your own floder that GEO provided
mkdir upload/the_folder_name_GEO_provided

### Create your a new folder for this submission
mkdir geo_submmsion_Jan04

### Go to the GEO FTP server selected folder 
cd upload/the_folder_name_GEO_proveid/geo_submmsion_Jan04

### Go to the local folder that contain your data
lcd ur_local_data_file

### Copy the data and md5 to the GEO FTP server
put *fastq.gz
put md5.txt

```

### Step3: Fill out the Metadata form
➊ Download the latest Metadata form and complete all the required (labeled in **) cells. 
> An example form is downloaded on the date January 2024.

<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_10.png?raw=true" width="100%"/>
</p>
<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_11.png?raw=true" width="100%"/>
</p>



### Step4: Submit your data
➊ After finishing data transfer, go to the next box, **Upload metadata**, and upload the selected subfolder, metadata, and choose a release date. 
<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_13.png?raw=true" width="100%"/>
</p>

➋ Finally, click **Submit**! Then you will receive an email if the upload is successful.
<p align="center">
<img src="https://github.com/beritlin/NGS_analyses/blob/main/Figure/GEO_12.png?raw=true" width="100%"/>
</p>

