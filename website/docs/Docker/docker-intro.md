---
title: Introduction for Docker
---

###  Images

check images

```bash
docker images 
docker images -a
```

remove images

```bash
docker rmi [ID]
```



### container

check container

```bash
docker ps ### show running ones
docker ps -a ### show all
```

first run docker image prune to clean up all dangling images

### run docker image

```bash
docker run -ti (image name) bash

```

### docker exec

```
docker exec (options) (container name) (command)

```


