---
title: Make Your Site More Visible on Google
---

Getting a good ranking in Google search results for your website may seem difficult. Google has some recommended steps for you to increase the visibility by letting the search engine aware the existence of your website.

## Register your site with Google Console

### step1: check if your site has been indexed
Simply search your site in the [Google search](https://google.com)
```
site:blog.peiyu.us
```
<img src="/img/doc/visible1.png" width="50%"/><img src="/img/doc/visible2.png" width="50%"/>

Don't worry if your site is not in the result **(left one)**. It will be solve after following the steps below.

### step2: submit URL for verification
You can then go to your only search result [Try Google Search Console](https://search.google.com/search-console/welcome?utm_source=about-page) and star your setting.
It is recommend to use the right one ` URL prefix ` which is easier and paste your site URL.
<p align="center"> <img src="/img/doc/visible3.png" width="100%"/></p>

Next, there are 5 different methods for you to choose. I'll recommend using the 3rd one **Google Analytics**, since we already set up it in the [previous doc](website-analysis.md).
<p align="center"> <img src="/img/doc/visible4.png" width="100%"/></p>

After verification, the site and basic analysis will shown in your Google search console.
<p align="center"> <img src="/img/doc/visible5.png" width="100%"/></p>

### step3: add site map
A sitemap is a file that providing information about the pages, documents, and other files on your site, and the relationships between them. Search engines like Google read this file to crawl your site more accurately and efficiently.

Docusaurus has the plugin ` plugin-sitemap ` can be directly used in the website. you can firstly install in in your ` package.json ` by

```bash
yarn add plugin-sitemap
```
> make sure the version is the same as your Docusaurus

and add the following code to your `docusaurus.config.js` file.

```jsx title="docusaurus.config.js"
module.exports = {
  presets: [
    [
      '@docusaurus/preset-classic',
      {
         // highlight-start
        sitemap: {
          changefreq: 'weekly',
          priority: 0.5,
          ignorePatterns: ['/tags/**'],
          filename: 'sitemap.xml',
        },
      },
       // highlight-end
    ],
  ],
};
```
You can find your sitemap at ` /sitemap.xml `.


As the influence of your website increase, the rank will be higher and higher in Google search.