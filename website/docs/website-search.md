---
title: Add Search Bar to Yor Website
---

The official search plugin fot Docusaurus is [**Algolia DocSearch**](https://docusaurus.io/docs/search#using-algolia-docsearch). It is free for open-source project only which means the website has to fit the requirement of [Algloia checklist](https://docsearch.algolia.com/docs/who-can-apply/). Once you get their official approval, you can start to set up in your website!

## Register your site to Algolia DocSearch

### step1: apply to the Algolia programme
Write down your application form in [Algolia](https://docsearch.algolia.com) and click `Join the programme`. Then just wait for their approval.

<img src="/img/doc/search1.png" width="50%"/><img src="/img/doc/search2.png" width="50%"/>

## Connect to Algolia 

### step2: add the job id to the script
After receiving the approve mail, you can then navigate to `docusaurus.config.js` and add the following code. All the needed IDs are in the mail!

```jsx title="docusaurus.config.js"
module.exports = {
  themeConfig: {
    algolia: {
      appId: 'YOUR_APP_ID',
      apiKey: 'YOUR_SEARCH_API_KEY',
      indexName: 'YOUR_INDEX_NAME',
      contextualSearch: true,
    },
  },
};
```

Algolia crawls your website once a week and aggregates all the content in an index for searching. 